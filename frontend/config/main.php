<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', \common\bootstraps\UserLoginBootstrap::class, 'systemLog'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require __DIR__ . '/rules.php',
        ],
        'systemLog' => [
            'enabled' => true,
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => ['app.js'],
                    'sourcePath' => '@frontend/web/js',
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],

            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
    ],
    'container' => [
        'definitions' => [
            yii\grid\ActionColumn::class => [
                'template' => '<div class="table-action">{update} {delete}</div>',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return \yii\bootstrap\Html::a('<i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i>', $url,
                            [
                            ]);
                    },
                    'delete' => function ($url, $model) {
                        return \yii\bootstrap\Html::a('<i class="align-middle mr-2 fas fa-fw fa-trash"></i>', $url, [
                            'data-method' => 'post',
                            'data-confirm' => 'Вы действительно хотите удалить?',
                        ]);
                    },
                ],
            ],
            \yii\jui\DatePicker::class => [
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
            ],
            \yii\widgets\Breadcrumbs::class => [
                'itemTemplate' => "<li class=\"breadcrumb-item\">{link}</li>\n",
                'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                'tag' => 'ol',
            ],
        ],
    ],
    'params' => $params,
];
