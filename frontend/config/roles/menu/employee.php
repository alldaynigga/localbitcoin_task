<?php
return [
    //["label" => "Home", "url" => "/", "icon" => "home"],
    //["label" => "Layout", "url" => ["site/layout"], "icon" => "files-o"],


    [
        'label' => \Yii::t('app.menu', 'Контрагенты'),
        'url' => '#contractor',
        'icon' => 'contractor',
        'submenuId' => 'contractor',
        'items' => [
            [
                'label' => \Yii::t('app.menu', 'Контрагенты'),
                'url' => ['/contractor/contractor/index'],
                'icon' => 'contractor',
            ],
            [
                'label' => \Yii::t('app.menu', 'Реестр планов доходов'),
                'url' => ['/contractor/contractor-income-plane/index'],
            ],

        ],
    ],
    /*[
        'label' => \Yii::t('app.menu','Справочники'),
        'url' => '#catalog',
        'icon' => 'catalog',
        'submenuId' => 'catalog',
        'items' => [
            [
                'label' => \Yii::t('app.menu','Реестр типов расходов'),
                'url' => ['/catalog/expense-type/index'],
                'icon' => 'catalog',
            ],
            [
                'label' => \Yii::t('app.menu','Категории товаров'),
                'url' => ['/catalog/category/index'],
                'icon' => 'category',
            ],
        ]
    ],*/
];
