<?php

namespace frontend\access;

use common\models\User;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * Класс для формирования меню в зависимости от роли пользователя
 */
class MenuBuilder
{
    /**
     * Возвращает список пунктов меню для текущего юзера с учетом его прав
     *
     * @return array
     * @throws ForbiddenHttpException
     */
    public static function getMenuItems(): array
    {
        if (\Yii::$app->user->isGuest) {
            return [];
        }
        $files = [];
        $user = \Yii::$app->user->identity;
        $allRoles = ArrayHelper::merge(User::getEmployeeRoles(), User::getManagerRoles());
        foreach ($allRoles as $role) {
            if ($user->hasAssignment($role)) {
                $files [] = \Yii::getAlias("@frontend/config/roles/menu/{$role}.php");
            }
        }

        if (!$files) {
            throw new ForbiddenHttpException();
        }
        $items = [];
        foreach ($files as $file) {
            $items = ArrayHelper::merge($items, require($file));
        }

        return $items;
    }
}
