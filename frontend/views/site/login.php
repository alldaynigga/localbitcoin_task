<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use common\widgets\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
<?= $form->field($model, 'username')->textInput(
    [
        'autofocus' => true,
        'class' => 'form-control form-control-lg',
        'placeholder' => 'Введите логин',
    ]
)->label('Логин') ?>
<?= $form->field($model, 'password', [])->passwordInput(
    [
        'class' => 'form-control form-control-lg',
        'placeholder' => 'Введите пароль',
    ]
)->label('Пароль') ?>
<?= $form->field($model, 'rememberMe')->checkbox(
    [
        'class' => 'custom-control-input',
    ]
)->label('Запомнить пароль', ['class' => "custom-control-label text-small"]) ?>
    <div class="text-center mt-3">
        <?= Html::submitButton('Войти', ['class' => 'btn btn-lg btn-primary', 'name' => 'login-button']) ?>
    </div>
<?php ActiveForm::end(); ?>
