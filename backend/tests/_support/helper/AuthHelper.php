<?php

namespace backend\tests\_support\helper;

use common\components\rbac\OSDBManager;
use Yii;

class AuthHelper
{
    public static function addUserRolesAndPermissions()
    {
        $authManager = Yii::createObject(OSDBManager::class);

        /**
         * Admin
         */

        $adminAccess = $authManager->getPermission('adminAccess');
        if (!$adminAccess) {
            $adminAccess = $authManager->createPermission('adminAccess');
            $authManager->add($adminAccess);
        }
        $adminAccess->description = 'Доступ к администрации';
        $authManager->update('adminAccess', $adminAccess);

        $adminRole = $authManager->getRole('admin');
        if (!$adminRole) {
            $adminRole = $authManager->createRole('admin');
            $authManager->add($adminRole);
        }
        $adminRole->description = 'Админ';
        $authManager->update('admin', $adminRole);
        if (!$authManager->canAddChild($adminRole, $adminAccess)) {
            $authManager->addChild($adminRole, $adminAccess);
        }


    }
}
