<?php

use backend\tests\FunctionalTester;
use common\tests\fixtures\UserFixture;
use common\tests\fixtures\SystemLogFixture;
use common\tests\fixtures\SystemLogDetailFixture;
use backend\tests\_support\Helper\AuthHelper;
use common\models\SystemLog;

class SystemLogCest
{
    public function _fixtures()
    {
        return [
            'user' => ['class' => UserFixture::class,],
            'user_login' => \common\tests\fixtures\UserLoginFixture::class,
            'system_log' => SystemLogFixture::class,
            'system_log_detail' => SystemLogDetailFixture::class,
        ];
    }

    public function _before()
    {
        AuthHelper::addUserRolesAndPermissions();
    }

    public function tryLoginLog(FunctionalTester $I, \Codeception\Module\Yii2 $module)
    {
        $adminUser = $I->grabFixture('user', 'admin');
        $adminUser->setAssignments(['admin' => 1]);
        $I->amOnPage('/site/login');
        $I->fillField('LoginForm[username]', 'admin');
        $I->fillField('LoginForm[password]', 'admin');
        $I->click('login-button');
        $I->see('Logout (admin)', 'form button[type=submit]');

        $systemLogModels = SystemLog::find()->orderBy(['id' => SORT_DESC])->limit(3)->all();
        $I->assertInstanceOf(SystemLog::class, $systemLogModels[2]);
        $startLoginModel = $systemLogModels[2];
        $I->assertInstanceOf(SystemLog::class, $systemLogModels[1]);
        $submitLoginModel = $systemLogModels[1];
        $I->assertInstanceOf(SystemLog::class, $systemLogModels[0]);
        $redirectLoginModel = $systemLogModels[0];
        $I->assertNull($startLoginModel->user_login_id);
        $params = \yii\helpers\Json::decode($submitLoginModel->params);
        $I->assertEquals($params['LoginForm']['username'], 'admin');
        $I->assertArrayNotHasKey('password', $params['LoginForm']);
        $I->assertNotNull($redirectLoginModel->user_login_id);
    }

}