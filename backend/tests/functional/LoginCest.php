<?php

namespace backend\tests\functional;

use backend\tests\FunctionalTester;
use common\models\UserLogin;
use common\tests\fixtures\UserFixture;

/**
 * Class LoginCest
 */
class LoginCest
{
    /**
     * Load fixtures before db transaction begin
     * Called in _before()
     * @see \Codeception\Module\Yii2::_before()
     * @see \Codeception\Module\Yii2::loadFixtures()
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    /**
     * @param FunctionalTester $I
     */
    public function loginUser(FunctionalTester $I)
    {
        $I->amOnPage('/site/login');
        $I->fillField('Логин', 'admin');
        $I->fillField('Пароль', 'admin');
        $I->click('login-button');

        $I->see('Logout (admin)', 'form button[type=submit]');
        $I->dontSeeLink('Login');
        $I->dontSeeLink('Signup');
        $userLoginModel = UserLogin::find()->orderBy(['id' => SORT_DESC])->one();
        $I->assertNotEmpty($userLoginModel);
        $admin = $I->grabFixture('user', 'admin');
        $I->assertEquals($userLoginModel->user_id, $admin->id);
    }
}
