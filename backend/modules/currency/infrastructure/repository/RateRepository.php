<?php

namespace backend\modules\currency\infrastructure\repository;


use backend\modules\currency\app\client\AsyncRequestsService;
use backend\modules\currency\app\client\request\BaseRequest;
use backend\modules\currency\app\client\request\Rate;
use backend\modules\currency\domain\interfaces\RateServiceInterface;
use backend\modules\currency\domain\value\RateValue;

/**
 * Class RateRepository
 * @package backend\modules\currency\infrastructure\repository
 */
class RateRepository implements RateServiceInterface
{

    public const TABLE = '{{%rate}}';

    /**
     * @var AsyncRequestsService
     */
    protected $asyncRequestService;

    /**
     * @return array
     */
    public function fetch(): array
    {
        $rows = (new \yii\db\Query())
            ->select(['amount', 'formula', 'created_at'])
            ->from(self::TABLE)
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->populate($rows);
    }

    /**
     * @param BaseRequest $request
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function load(BaseRequest $request): void
    {
        $this->asyncRequestService = \Yii::$container->get(AsyncRequestsService::class);
        /** @type BaseRequest $request */
        $this->asyncRequestService->setRequest($request);
        /** @type AccountRepository $accountRepository */
        $accountRepository = \Yii::$container->get(AccountRepository::class);
        $accounts = $accountRepository->getAccForSingleRequest();
        $this->asyncRequestService->setAccounts($accounts);
        $this->asyncRequestService->sendRequests();
    }

    /**
     * @inheritdoc
     */
    public function save(RateValue $rate): void
    {
        \Yii::$app->db->createCommand()->insert(self::TABLE, [
            'amount' => $rate->getAmount(),
            'formula' => $rate->getFormula(),
        ])->execute();
    }

    /**
     * @param $items
     * @return RateValue[]
     */
    protected function populate(array $items): array
    {
        return array_map(
            function ($item) {
                return \Yii::$container->get(RateValue::class, [
                    (float)$item['amount'],
                    $item['formula'],
                    $item['created_at'],
                ]);
            }
            , $items);
    }
}
