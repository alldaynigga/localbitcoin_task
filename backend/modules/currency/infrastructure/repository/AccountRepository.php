<?php

namespace backend\modules\currency\infrastructure\repository;


use backend\modules\currency\domain\entity\Account;
use backend\modules\currency\domain\entity\Rate;
use backend\modules\currency\domain\interfaces\AccountServiceInterface;
use backend\modules\currency\domain\value\AccountValue;

/**
 * Class AccountsRepository
 * @package backend\modules\currency\infrastructure\repository
 */
class AccountRepository implements AccountServiceInterface
{
    /**
     * Table name
     */
    public const TABLE = '{{%account}}';


    public const SYSTEM_ACCOUNT_ID = 1;

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function fetchForLoad(): array
    {
        $rows = (new \yii\db\Query())
            ->from(self::TABLE)
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(10)
            ->all();

        return $this->populate($rows);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getAccForSingleRequest(): array
    {
        $row = (new \yii\db\Query())
            ->from(self::TABLE)
            ->where(['id' => self::SYSTEM_ACCOUNT_ID])
            ->one();

        return $this->populate([$row]);
    }

    /**
     * @param AccountValue $accountValue
     */
    public function save(AccountValue $accountValue)
    {
        \Yii::$app->db->createCommand()
            ->insert(self::TABLE, [
                'login' => $accountValue->getLogin(),
                'api_key' => $accountValue->getApiKey(),
                'secret_key' => $accountValue->getSecretKey(),
            ])->execute();
    }

    /**
     * @param array $items
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function populate(array $items): array
    {
        $result = [];
        foreach ($items as $item) {
            /** @type AccountValue $accountValue */
            $accountValue = \Yii::$container->get(AccountValue::class,
                [$item['login'], $item['api_key'], $item['secret_key']]);
            $account = \Yii::$container->get(Account::class, [$accountValue, $item['id']]);
            $result[] = $account;
        }

        return $result;
    }
}
