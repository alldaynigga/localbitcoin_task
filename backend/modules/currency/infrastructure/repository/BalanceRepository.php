<?php

namespace backend\modules\currency\infrastructure\repository;


use backend\modules\currency\app\client\AsyncRequestsService;
use backend\modules\currency\app\client\request\WalletBalance;
use backend\modules\currency\domain\aggregator\BalancePresentation;
use backend\modules\currency\domain\entity\Account;
use backend\modules\currency\domain\interfaces\BalanceServiceInterface;
use backend\modules\currency\domain\value\AccountValue;
use backend\modules\currency\domain\value\BalanceAmount;

class BalanceRepository implements BalanceServiceInterface
{
    public const TABLE = '{{%balance}}';

    /**
     * @var AsyncRequestsService
     */
    protected $asyncRequestService;

    public function fetch(): array
    {
        $rows = (new \yii\db\Query())
            ->select(['amount', 'login', 'balance.created_at as created_at'])
            ->from(self::TABLE)
            ->innerJoin('{{%account}}', 'account_id = account.id')
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->populate($rows);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function load(): void
    {
        $this->asyncRequestService = \Yii::$container->get(AsyncRequestsService::class);
        /** @type WalletBalance $request */
        $request = \Yii::$container->get(WalletBalance::class);
        $this->asyncRequestService->setRequest($request);
        /** @type AccountRepository $accountRepository */
        $accountRepository = \Yii::$container->get(AccountRepository::class);
        $accounts = $accountRepository->fetchForLoad();
        $this->asyncRequestService->setAccounts($accounts);
        $this->asyncRequestService->sendRequests();
    }

    /**
     * @param BalanceAmount $balanceAmount
     * @param Account $account
     * @param int $accountId
     */
    public function save(BalanceAmount $balanceAmount, Account $account)
    {
        $command = \Yii::$app->db->createCommand();
        $command->insert(self::TABLE, [
            'amount' => $balanceAmount->getAmount(),
            'account_id' => $account->getId(),
        ])->execute();
    }

    /**
     * @param $items
     * @return BalancePresentation[]
     */
    protected function populate(array $items): array
    {
        return array_map(
            function ($item) {
                $balance = \Yii::$container->get(BalanceAmount::class, [(float)$item['amount'], $item['created_at']]);
                $account = \Yii::$container->get(AccountValue::class, [$item['login']]);

                return \Yii::$container->get(BalancePresentation::class, [$balance, $account]);
            }
            , $items);
    }
}
