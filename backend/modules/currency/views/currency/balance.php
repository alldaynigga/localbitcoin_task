<?php

use yii\helpers\Html;
use common\widgets\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var  $rows \backend\modules\currency\infrastructure\repository\BalancePresentation[] */
/** @var $balance \backend\modules\currency\infrastructure\repository\BalancePresentation */

$this->title = 'Баланс';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="h3 mb-3"><?= $this->title ?></h1>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div id="datatables-basic_wrapper"
                     class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="dataTables_length"
                                 id="datatables-basic_length">
                                <button class="btn btn-secondary" onclick=""><i
                                            class="align-middle"
                                            data-feather="filter"></i> <span
                                            class="align-middle"></button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <!--<div class="dataTables_length right-float" id="datatables-basic_length">
                                <label>По
                                    <select name="datatables-basic_length" aria-controls="datatables-basic" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> элементов
                                </label>
                            </div>-->
                            <!--<div id="datatables-basic_filter" class="dataTables_filter">
                                <label>
                                <div class="input-group mb-3">
                                    <input type="search" class="form-control" placeholder="">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="align-middle" data-feather="search"></i> <span class="align-middle"></span></span>
                                    </div>
                                </div>
                                </label>
                            </div>-->
                        </div>
                    </div>
                    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
                    <table class="table table-striped dataTable dtr-inline">
                        <thead>
                        <tr>
                            <td>Логин</td>
                            <td>Баланс</td>
                            <td>Дата</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $balance) { ?>
                            <tr>
                                <td><?= $balance->getAccount()->getLogin() ?></td>
                                <td><?= $balance->getBalance()->getAmount() ?></td>
                                <td><?= $balance->getBalance()->getCreatedAt() ?></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
