<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Добавить аккаунт';
$this->params['breadcrumbs'][] = ['label' => 'Local bitcoin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

