<?php

use yii\helpers\Html;
use common\widgets\ActiveForm;
use common\models\Driver;

/* @var $this yii\web\View */
/* @var $model \backend\modules\currency\forms\AccountForm */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="card">
    <div data-role="toggle-block">
        <div class="card-header">
            <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
            <h6 class="card-title">
                <button class="btn btn-sm" data-role="toggle-button"><i class="align-middle"
                                                                        data-feather="minus-square"></i><i
                            class="align-middle" data-feather="plus-square" style="display: none;"></i> <span
                            class="align-middle">Основная информация</button>
            </h6>
        </div>
        <div class="card-body" data-role="toggle">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'api_key')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'secret_key')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <div data-role="toggle-block">
        <div class="card-body" data-role="toggle">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
