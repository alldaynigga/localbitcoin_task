<?php

namespace backend\modules\currency\controllers;

use backend\access\AdminController;
use backend\modules\currency\forms\AccountForm;
use backend\modules\currency\infrastructure\repository\BalanceRepository;
use backend\modules\currency\infrastructure\repository\RateRepository;
use Yii;
use common\models\search\CarSearch;
use yii\filters\VerbFilter;

/**
 * CarController implements the CRUD actions for Car model.
 */
class CurrencyController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionBalance()
    {
        /** @type BalanceRepository $balanceRepository */
        $balanceRepository = \Yii::$container->get(BalanceRepository::class);
        $rows = $balanceRepository->fetch();

        return $this->render('balance', [
            'rows' => $rows,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionRate()
    {
        /** @type RateRepository $rateRepository */
        $rateRepository = \Yii::$container->get(RateRepository::class);
        $rows = $rateRepository->fetch();

        return $this->render('rate', [
            'rows' => $rows,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = \Yii::$container->get(AccountForm::class);
        if ($model->saveData(Yii::$app->request->post())) {
            return $this->redirect(['balance']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


}
