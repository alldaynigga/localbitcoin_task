<?php

namespace backend\modules\currency\domain\entity;

use backend\modules\currency\domain\value\RateValue;

class Rate
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var RateValue
     */
    protected $rate;

    /**
     * @var RateValue
     */
    protected $createdAt;

    /**
     * @return RateValue
     */
    public function getRate()
    {
        return $this->rate;
    }

    public function __construct(RateValue $rate)
    {
        $this->rate = $rate;
    }

}
