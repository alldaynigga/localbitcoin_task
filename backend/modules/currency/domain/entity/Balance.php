<?php

namespace backend\modules\currency\domain\entity;

use backend\modules\currency\domain\value\BalanceAmount;

class Balance
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var BalanceAmount
     */
    protected $balance;

    /**
     * @return BalanceAmount
     */
    public function getBalance(): BalanceAmount
    {
        return $this->balance;
    }

    /**
     * BalanceAmount constructor.
     * @param BalanceAmount $amount
     */
    public function __construct(BalanceAmount $amount)
    {
        $this->amount = $amount;
    }
}
