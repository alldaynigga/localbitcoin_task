<?php

namespace backend\modules\currency\domain\entity;


use backend\modules\currency\domain\value\AccountValue;

/**
 * Class Account
 * @package backend\modules\currency\domain\entity
 */
class Account
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var AccountValue
     */
    protected $account;

    /**
     * @return AccountValue
     */
    public function getAccount(): AccountValue
    {
        return $this->account;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Account constructor.
     * @param AccountValue $account
     */
    public function __construct(AccountValue $account, $id = null)
    {
        $this->id = $id;
        $this->account = $account;
    }

}
