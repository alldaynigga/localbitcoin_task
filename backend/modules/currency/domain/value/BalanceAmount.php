<?php

namespace backend\modules\currency\domain\value;


/**
 * Class BalanceAmount
 * @package backend\modules\currency\domain\value
 */
class BalanceAmount
{

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $created_at;

    /**
     * @return mixed
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): ?string
    {
        return !empty($this->created_at) ? date('d.m.Y H:i:s', strtotime($this->created_at)) : null;
    }

    /**
     * BalanceAmount constructor.
     * @param float $amount
     */
    public function __construct(?float $amount = null, ?string $created_at = null)
    {
        $this->amount = $amount;
        $this->created_at = $created_at;
    }
}
