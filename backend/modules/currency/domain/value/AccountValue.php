<?php

namespace backend\modules\currency\domain\value;


class AccountValue
{

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $secretKey;

    /**
     * @return string
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getSecretKey(): ?string
    {
        return $this->secretKey;
    }

    /**
     * BalanceAmount constructor.
     * @param int $amount
     */
    public function __construct(string $login = null, string $apiKey = null, string $secretKey = null)
    {
        $this->login = $login;
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
    }

}
