<?php

namespace backend\modules\currency\domain\value;


class RateValue
{

    /**
     * @var int
     */
    protected $amount;

    /**
     * @var int
     */
    protected $formula;

    /**
     * @var
     */
    protected $created_at;

    /**
     * @return int
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): ?string
    {
        return !empty($this->created_at) ? date('d.m.Y H:i:s', strtotime($this->created_at)) : null;
    }


    /**
     * @return string
     */
    public function getFormula(): ?string
    {
        return $this->formula;
    }

    /**
     * BalanceAmount constructor.
     * @param int $amount
     */
    public function __construct(int $amount, string $formula, $created_at = null)
    {
        $this->amount = $amount;
        $this->formula = $formula;
        $this->created_at = $created_at;
    }

}
