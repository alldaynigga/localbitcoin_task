<?php

namespace backend\modules\currency\domain\aggregator;


use backend\modules\currency\domain\value\AccountValue;
use backend\modules\currency\domain\value\BalanceAmount;

/**
 * Class BalancePresentation
 * @package backend\modules\currency\domain\aggregator
 */
class BalancePresentation
{

    /**
     * @var BalanceAmount
     */
    protected $balance;

    /**
     * @var AccountValue
     */
    protected $account;

    /**
     * BalancePresentation constructor.
     * @param BalanceAmount $balanceAmount
     * @param AccountValue $accountValue
     */
    public function __construct(BalanceAmount $balanceAmount, AccountValue $accountValue)
    {
        $this->balance = $balanceAmount;
        $this->account = $accountValue;
    }

    /**
     * @return BalanceAmount
     */
    public function getBalance(): BalanceAmount
    {
        return $this->balance;
    }

    /**
     * @return AccountValue
     */
    public function getAccount(): AccountValue
    {
        return $this->account;
    }

}
