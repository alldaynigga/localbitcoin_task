<?php

namespace backend\modules\currency\domain\interfaces;

use backend\modules\currency\domain\entity\Account;
use backend\modules\currency\domain\entity\Balance;
use backend\modules\currency\domain\value\BalanceAmount;

interface BalanceServiceInterface
{

    /**
     * @return Balance[]
     */
    public function fetch(): array;

    /**
     * @return void
     * @throws \Exception
     */
    public function load(): void;

    /**
     * @param BalanceAmount $balanceAmount
     * @param Account $account
     * @return void
     * @throws \Exception
     */
    public function save(BalanceAmount $balanceAmount, Account $account);

}
