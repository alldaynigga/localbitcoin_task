<?php

namespace backend\modules\currency\domain\interfaces;

use backend\modules\currency\domain\entity\Rate;
use backend\modules\currency\domain\value\AccountValue;

interface AccountServiceInterface
{


    /**
     * @return array
     */
    public function fetchForLoad(): array;

    /**
     * @return mixed
     */
    public function save(AccountValue $accountValue);

}
