<?php

namespace backend\modules\currency\domain\interfaces;

use backend\modules\currency\app\client\request\BaseRequest;
use backend\modules\currency\domain\entity\Rate;
use backend\modules\currency\domain\value\RateValue;

interface RateServiceInterface
{

    /**
     * @return mixed
     */
    public function fetch(): array;

    /**
     * @return void
     */
    public function load(BaseRequest $request): void;


    /**
     * @throws \Exception
     * @return void
     */
    public function save(RateValue $rate): void;

}
