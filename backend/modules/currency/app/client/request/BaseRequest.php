<?php

namespace backend\modules\currency\app\client\request;


use GuzzleHttp\Psr7\Response;

/**
 * Class WalletBalance
 * @package backend\modules\currency\app\client\request
 */
abstract class BaseRequest
{

    public const METHOD_POST = 'POST';

    public const METHOD_GET = 'GET';

    public const RESPONSE_OK = 200;

    /**
     * @return string
     */
    abstract public function getUri(): string;

    /**
     * @return string
     */
    abstract public function getMethod(): string;

    /**
     *
     */
    abstract function fullFilled($data): callable;

    /**
     * @param $response
     * @return null|object
     */
    protected function parseRawBody($response)
    {
        if (empty($response) || !($response instanceof Response)
            || $response->getStatusCode() !== self::RESPONSE_OK) {
            throw new \Exception('Wrong response state');
        }
        $body = json_decode((string)$response->getBody());
        if (empty($body)) {
            throw new \Exception('Wrong response json format');
        }
        $rawBody = json_decode((string)$response->getBody());
        if (empty($rawBody) || !isset($rawBody->data)) {
            throw new \Exception('Wrong response data');
        }

        return $rawBody->data;
    }
}
