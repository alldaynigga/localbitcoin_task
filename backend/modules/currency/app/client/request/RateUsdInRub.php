<?php

namespace backend\modules\currency\app\client\request;


/**
 * Class RateUsdInRub
 * @package backend\modules\currency\app\client\request
 */
class RateUsdInRub extends Rate
{

    /**
     * @return string
     */
    protected function getFormula(): string
    {
        return 'usd_in_rub';
    }

}
