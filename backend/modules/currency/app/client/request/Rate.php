<?php

namespace backend\modules\currency\app\client\request;


use backend\modules\currency\domain\value\BalanceAmount;
use backend\modules\currency\domain\value\RateValue;
use backend\modules\currency\infrastructure\repository\RateRepository;

/**
 * Class Rate
 * @package backend\modules\currency\app\client\request
 */
abstract class Rate extends BaseRequest
{

    protected const EQUATION_FORMAT_URI = '/equation/%s/';


    /**
     * @return string
     */
    public function getUri(): string
    {
        return sprintf(self::EQUATION_FORMAT_URI, $this->getFormula());
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return self::METHOD_GET;
    }

    /**
     * @param $data
     * @return callable
     */
    public function fullFilled($data): callable
    {
        return function ($response, $index) {
            $rawBody = $this->parseRawBody($response);
            if ($rawBody === null) {
                throw new \Exception('Wrong balance response');
            }
            $amount = (float)$rawBody;
            /** @type RateRepository $rateRepository */
            $rateRepository = \Yii::$container->get(RateRepository::class);
            /** @type RateValue $rate */
            $rate = \Yii::$container->get(RateValue::class, [$amount, $this->getFormula()]);
            $rateRepository->save($rate);
        };
    }

    /**
     * @return string
     */
    abstract protected function getFormula(): string;

}
