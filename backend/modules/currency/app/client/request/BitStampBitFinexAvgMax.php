<?php

namespace backend\modules\currency\app\client\request;


/**
 * Class RateUsdInRub
 * @package backend\modules\currency\app\client\request
 */
class BitStampBitFinexAvgMax extends Rate
{

    /**
     * @return string
     */
    protected function getFormula(): string
    {
        return 'max(bitstampusd_avg,bitfinexusd_avg)*usd_in_rub';
    }

}
