<?php

namespace backend\modules\currency\app\client\request;


use backend\modules\currency\domain\value\AccountValue;
use backend\modules\currency\domain\value\BalanceAmount;
use backend\modules\currency\infrastructure\repository\BalanceRepository;

/**
 * Class WalletBalance
 * @package backend\modules\currency\app\client\request
 */
class WalletBalance extends BaseRequest
{

    /**
     * @return string
     */
    public function getUri(): string
    {
        return '/wallet-balance/';
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return self::METHOD_GET;
    }

    /**
     * @param $data
     * @return callable
     */
    public function fullFilled($data): callable
    {
        return function ($response, $index) use ($data) {
            $rawBody = $this->parseRawBody($response);
            if ($rawBody === null || !isset($rawBody->total->balance) || !is_numeric($rawBody->total->balance)) {
                throw new \Exception('Wrong balance response');
            }
            $balance = (float)$rawBody->total->balance;
            /** @type BalanceRepository $balanceRepository */
            $balanceRepository = \Yii::$container->get(BalanceRepository::class);
            /** @type BalanceAmount $amount */
            $amount = \Yii::$container->get(BalanceAmount::class, [$balance]);
            $balanceRepository->save($amount, $data[$index]);
        };
    }

}
