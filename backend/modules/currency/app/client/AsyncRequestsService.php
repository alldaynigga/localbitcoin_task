<?php

namespace backend\modules\currency\app\client;

use backend\modules\currency\app\client\request\BaseRequest;
use backend\modules\currency\domain\entity\Account;
use backend\modules\currency\domain\value\AccountValue;
use \GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;

/**
 * Class LocalBitcoinService
 * @package backend\modules\currency\app\client
 */
class AsyncRequestsService
{
    protected const APIAUTH_KEY = 'Apiauth-Key';
    protected const APIAUTH_NONCE = 'Apiauth-Nonce';
    protected const APIAUTH_SIGNATURE = 'Apiauth-Signature';

    protected const URI_PREFIX = '/api';
    //protected const URI_PREFIX = '';

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var BaseRequest
     */
    protected $request;

    /**
     * @var Account[]
     */
    protected $accounts = [];

    /**
     * LocalBitcoinService constructor.
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function __construct()
    {
        $this->client = \Yii::$container->get(Client::class, [
            [
                //'curl'            => [ CURLOPT_SSL_VERIFYPEER => TRUE, CURLOPT_SSL_VERIFYHOST => false ],
                'base_uri' => 'https://localbitcoins.net',
                //'base_uri' => 'http://otpravka.bte-atm.ru',
                'concurrency' => 20,
                'curl' => [CURLOPT_RETURNTRANSFER => true]
                //'default' => ['verify' => false],
                //'verify' => 'false'
            ],
        ]);
    }

    /**
     * @param BaseRequest $request
     */
    public function setRequest(BaseRequest $request): void
    {
        $this->request = $request;
    }

    /**
     * @param array $accounts
     */
    public function setAccounts(array $accounts): void
    {
        $this->accounts = $accounts;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function sendRequests(): void
    {
        \Yii::$container->get(Pool::class, [
            $this->client,
            $this->getRequests(),
            [
                'fulfilled' => $this->request->fullFilled($this->accounts),
                'rejected' => function ($reason, $index) {
                    return [];
                },
            ],
        ])->promise()->wait();
    }

    /**
     * @param array $items
     * @return \Generator
     * @throws \Exception
     */
    protected function getRequests(): \Generator
    {
        $uri = self::URI_PREFIX . $this->request->getUri();
        foreach ($this->accounts as $i => $account) {
            yield \Yii::$container->get(
                Request::class,
                [
                    $this->request->getMethod(),
                    $uri,
                    $this->getHeaders($account->getAccount(), $i),
                ]
            );
        }
    }

    /**
     * @param AccountValue $accountValue
     * @param $nonce
     * @return array
     */
    protected function getHeaders(AccountValue $accountValue, $nonce)
    {
        $mt = explode(' ', microtime());
        $nonce = $mt[1] . substr($mt[0], 2, 6);

        return [
            self::APIAUTH_NONCE => $nonce,
            self::APIAUTH_KEY => strtoupper($accountValue->getApiKey()),
            self::APIAUTH_SIGNATURE => $this->getSignature(
                $nonce,
                $accountValue->getApiKey(),
                $accountValue->getSecretKey(),
                $this->request->getUri()
            ),
        ];
    }

    /**
     * @param int $nonce
     * @param string $key
     * @param string $secret
     * @param string $uri
     * @param string $params
     * @return string
     */
    protected function getSignature(
        int $nonce,
        string $key,
        string $secret,
        string $uri,
        string $params = ''
    ): string {
        $msg = $nonce . $key . self::URI_PREFIX . $uri . $params;

        return strtoupper(hash_hmac('sha256', $msg, $secret));
    }

}
