<?php

namespace backend\modules\currency\forms;

use backend\modules\currency\domain\value\AccountValue;
use backend\modules\currency\infrastructure\repository\AccountRepository;
use common\models\ObjectedModel;

/**
 * Модель формы водителя
 */
class AccountForm extends ObjectedModel
{

    public $login;
    public $api_key;
    public $secret_key;

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'login',
            'api_key',
            'secret_key',
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'api_key' => 'Api Key',
            'secret_key' => 'Secret Key',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'login',
                    'api_key',
                    'secret_key',
                ],
                'string',
            ],
            [
                [
                    'login',
                    'api_key',
                    'secret_key',
                ],
                'required',
            ],
        ];
    }


    /**
     * Сохраняет форму пользователя
     *
     * @param array $data параметры запроса
     *
     * @return bool результат сохранения
     * @throws \yii\db\Exception
     * @throws \yii\base\Exception
     */
    public function saveData(array $data): bool
    {
        if ($this->load($data)) {
            if ($this->validate()) {
                /** @type AccountRepository $account */
                $accountRepository = \Yii::$container->get(AccountRepository::class);
                $account = \Yii::$container->get(AccountValue::class, [
                    $this->login,
                    $this->api_key,
                    $this->secret_key,
                ]);
                $accountRepository->save($account);

                return true;
            }
        }

        return false;
    }

}
