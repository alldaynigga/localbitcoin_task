<?php

namespace backend\modules\user\forms;

use common\models\AuthItem;
use common\models\ObjectParams;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;

/**
 * Модель формы user
 */
class UserForm extends User
{
    public $password;
    public $role;
    public $emails;
    public $phones;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['first_name', 'last_name', 'email', 'phone'], 'required'];
        $rules[] = [['password'], 'required', 'on' => static::SCENARIO_CREATE];
        $rules[] = ['password', 'string', 'min' => 3, 'max' => 50];
        $rules[] = ['role', 'string'];
        $rules[] = [
            'role',
            'exist',
            'skipOnEmpty' => true,
            'targetClass' => AuthItem::class,
            'targetAttribute' => ['role' => 'name'],
        ];

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['password'] = \Yii::t('app', 'Пароль');
        $labels['role'] = \Yii::t('app', 'Роль');
        $labels['emails'] = \Yii::t('app', 'Дополнительный email');
        $labels['phones'] = \Yii::t('app', 'Дополнительный телефон');

        return $labels;
    }

    /**
     * Переводим телефоны и email из параметров в массивы
     * {@inheritdoc}
     */
    public function afterFind()
    {
        if ($this->emails = ObjectParams::getValue($this->object_id, 'emails')) {
            $this->emails = $this->emails->toArray();
        }
        if ($this->phones = ObjectParams::getValue($this->object_id, 'phones')) {
            $this->phones = $this->phones->toArray();
        }
        parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (($insert || !empty($changedAttributes['role'])) && $this->role) {
            $this->setRole($this->role);
        }
    }

    /**
     * Блокируем возвращение пароля пользователя
     *
     * @return string возвращаемый пароль
     */
    public function getPassword(): ?string
    {
        return null;
    }

    /**
     * Устанавливает роль пользователя
     *
     * @param string $role роль пользователя
     *
     * @return void
     * @throws \Exception
     */
    public function setRole(string $role): void
    {
        $authManager = \Yii::$app->authManager;
        $authManager->revokeAll($this->getId());
        $authManager->assign($role, $this->getId());
    }

    /**
     * Возвращает список ролей для пользователя
     *
     * @return array
     */
    public static function getRoles(): array
    {
        return ArrayHelper::map(
            AuthItem::find()->andWhere(['type' => Item::TYPE_ROLE])->all(),
            'name',
            function (AuthItem $item) {
                return $item->description ?: $item->name;
            }
        );
    }

    /**
     * Сохраняет форму пользователя
     *
     * @param array $data параметры запроса
     *
     * @return bool результат сохранения
     * @throws \yii\db\Exception
     * @throws \yii\base\Exception
     */
    public function saveData(array $data): bool
    {
        if ($this->load($data)) {
            $transaction = self::getDb()->beginTransaction();

            if (!empty($this->password)) {
                $this->setPassword($this->password);
            }
            $saveStatus = $this->save();

            if ($saveStatus) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }

            return $saveStatus;
        }

        return false;
    }

    /**
     * Возвращает список возможных параметров
     *
     * @return array возможные параметры
     */
    public static function getParams(): array
    {
        return [
            'emails',
            'phones',
        ];
    }

    /**
     * Возвращает имя класса для поиска сущности по словарю
     *
     * @return string имя класса сохраняемой модели
     */
    protected static function getClassNamespase(): string
    {
        return User::class;
    }

}
