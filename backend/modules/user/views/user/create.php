<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Новый пользователь';
$this->params['breadcrumbs'][] = [
    'label' => 'Реестр пользователей',
    'url' => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

