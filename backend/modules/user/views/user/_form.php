<?php

use yii\helpers\Html;
use common\widgets\ActiveForm;
use common\widgets\BlockInputWidget;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div data-role="toggle-block">
            <div class="card-header">
                <h4 class="card-title"><?= Html::encode($this->title) ?></h4>
                <h6 class="card-title">
                    <button class="btn btn-sm" data-role="toggle-button"><i
                                class="align-middle"
                                data-feather="minus-square"></i><i
                                class="align-middle" data-feather="plus-square"
                                style="display: none;"></i> <span
                                class="align-middle">Основная информация
                    </button>
                </h6>
            </div>
            <div class="card-body" data-role="toggle">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'last_name')
                            ->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'first_name')
                            ->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'middle_name')
                            ->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'role')
                            ->dropDownList(\common\models\AuthItem::getRolesList()) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'username')
                            ->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'password')
                            ->passwordInput() ?>
                    </div>
                </div>
            </div>
        </div>
        <div data-role="toggle-block">
            <div class="card-header">
                <h6 class="card-title">
                    <button class="btn btn-sm" data-role="toggle-button"><i
                                class="align-middle"
                                data-feather="minus-square"></i><i
                                class="align-middle" data-feather="plus-square"
                                style="display: none;"></i> <span
                                class="align-middle">Контанты</button>
                </h6>
            </div>
            <div class="card-body" data-role="toggle">
                <?= BlockInputWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'email',
                    'arrayAttribute' => 'emails',
                ]) ?>
                <?= BlockInputWidget::widget([
                    'form' => $form,
                    'model' => $model,
                    'attribute' => 'phone',
                    'arrayAttribute' => 'phones',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить',
                        ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>