<?php

use common\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\search\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<hr>
<div class="row hide" data-role="toggle" <?= Yii::$app->request->isAjax ? ''
    : 'style="display: none"' ?>>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'get',
                    'options' => [
                        'data-pjax' => true,
                        'data-role' => 'search-form',
                    ],
                ]); ?>
                <div class="form-row">
                    <?= $form->field($model, 'id',
                        ['options' => ['class' => 'form-group col-md-6']])
                        ->label('Номер(id)') ?>

                    <?= $form->field($model, 'username',
                        ['options' => ['class' => 'form-group col-md-6']]) ?>
                </div>
                <div class="form-row">
                    <?= $form->field($model, 'name',
                        ['options' => ['class' => 'form-group col-md-12']])
                        ->label('ФИО') ?>

                </div>
                <div class="form-row">
                    <?= $form->field($model, 'email',
                        ['options' => ['class' => 'form-group col-md-6']]) ?>
                    <?= $form->field($model, 'phone',
                        ['options' => ['class' => 'form-group col-md-6']]) ?>
                </div>
                <!--<div class="form-row">
                <? /*= $form->field($model, 'status_id', ['options' => ['class' => 'form-group col-md-6']])->dropDownList(\common\models\User::getStatusLabels()) */ ?>
                <? /*= $form->field($model, 'role', ['options' => ['class' => 'form-group col-md-6']])->dropDownList(\common\models\AuthItem::getRolesList())->label('Роль') */ ?>
            </div>-->

                <div class="form-group">
                    <?= Html::submitButton('Поиск',
                        ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton('Сбросить',
                        ['class' => 'btn btn-secondary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>
