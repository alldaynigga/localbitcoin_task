<?php

use yii\helpers\Html;
use common\widgets\GridView;
use yii\widgets\Pjax;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи системы';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1 class="h3 mb-3"><?= $this->title ?></h1>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div id="datatables-basic_wrapper"
                     class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="dataTables_length"
                                 id="datatables-basic_length">
                                <?= Html::a('Добавить пользователя', ['create'],
                                    ['class' => 'btn btn-outline-success']) ?>
                                <button class="btn btn-secondary" onclick=""><i
                                            class="align-middle"
                                            data-feather="filter"></i> <span
                                            class="align-middle"></button>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <!--<div class="dataTables_length right-float" id="datatables-basic_length">
                                <label>По
                                    <select name="datatables-basic_length" aria-controls="datatables-basic" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> элементов
                                </label>
                            </div>-->
                            <!--<div id="datatables-basic_filter" class="dataTables_filter">
                                <label>
                                <div class="input-group mb-3">
                                    <input type="search" class="form-control" placeholder="">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="align-middle" data-feather="search"></i> <span class="align-middle"></span></span>
                                    </div>
                                </div>
                                </label>
                            </div>-->
                        </div>
                    </div>
                    <?php echo $this->render('_search',
                        ['model' => $searchModel]); ?>
                    <?php Pjax::begin(['timeout' => 5000]); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'label' => 'Номер(id)',
                            ],
                            [
                                'attribute' => 'name',
                                'content' => function (
                                    User $model
                                ) {
                                    return $model->getFullName();
                                },
                                'label' => 'ФИО',
                            ],
                            [
                                'attribute' => 'role',
                                'content' => function (
                                    User $model
                                ) {
                                    return $model->getRoleDescription();
                                },
                                'label' => 'Роль',
                            ],
                            [
                                'attribute' => 'phone',
                                'label' => 'Телефон',
                                'content' => function (
                                    User $model
                                ) {
                                    $phones = implode('<br>', $model->phones
                                        ? $model->phones->toArray() : []);

                                    return $model->phone . ($phones ? '<br>'
                                            . '<span>' . $phones . '</span>' : '');
                                },
                            ],
                            [
                                'attribute' => 'email',
                                'label' => 'Email',
                                'content' => function (
                                    User $model
                                ) {
                                    $emails = implode('<br>', $model->emails
                                        ? $model->emails->toArray() : []);

                                    return $model->email . ($emails ? '<br>'
                                            . '<span>' . $emails . '</span>' : '');
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'content' => function (
                                    User $model
                                ) {
                                    return $model->getStatusLabel();
                                },
                                'label' => 'Статус',
                            ],

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
