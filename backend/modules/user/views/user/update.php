<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Изменить профиль пользователя: ' . $model->id;
$this->params['breadcrumbs'][] = [
    'label' => 'Реестр пользователей',
    'url' => ['index'],
];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => ['view', 'id' => $model->id],
];
$this->params['breadcrumbs'][] = 'Изменить профиль пользователя';
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

