<?php

namespace backend\access;

use common\models\User;
use yii\web\ForbiddenHttpException;

/**
 * Класс для формирования меню в зависимости от роли пользователя
 */
class MenuBuilder
{
    /**
     * Возвращает список пунктов меню для текущего юзера с учетом его прав
     *
     * @return array
     * @throws ForbiddenHttpException
     */
    public static function getMenuItems(): array
    {
        if (\Yii::$app->user->isGuest) {
            return [];
        }

        $user = \Yii::$app->user->identity;

        if ($user->hasAssignment(User::ROLE_ADMIN)) {
            $role = User::ROLE_ADMIN;
        } else {
            return [];
        }

        $file = \Yii::getAlias("@backend/config/roles/menu/{$role}.php");

        if (!file_exists($file)) {
            throw new ForbiddenHttpException();
        }

        $items = require($file);

        return $items;
    }
}