$(document).ready(function () {
    $(document.body).on('change', '[data-role=main-select]', function () {
        var catgoryId = $(this).val();
        if (catgoryId) {
            $.post('/catalog/category/list?id=' + catgoryId,
                function (data) {
                    $('[data-role=sub-select] option').not(':first').remove();
                    $('[data-role=level-select]').val(2);
                    $.each(data, function (key, value) {
                        $("[data-role=sub-select]").append('<option value="' + key + '">' + value + '</option>');
                    });
                });
        } else {
            $('[data-role=sub-select] option').not(':first').remove();
            $('[data-role=level-select]').val(1);
        }
    });
    $(document.body).on('change', '[data-role=sub-select]', function () {
        $('[data-role=level-select]').val(3);
    });
});
