$(function () {
    $(".btn-secondary").click(function () {
        $("[data-role=toggle]").toggle();
    });
    $(document).on('submit', '[data-role=search-form]', function (event) {
        event.preventDefault(); // stop default submit behavior

        $.pjax.submit(event, '#p0', {
            'push': true,
            'replace': false,
            'timeout': 5000,
            //'scrollTo': 0,
            'maxCacheLength': 0,
        });
    });
    $("[data-role=toggle-button]").click(function () {
        $(this).parents("[data-role=toggle-block]").find("[data-role=toggle]").toggle();
        $(this).find("svg").toggle();
        return false;
    });

    $("[data-role=clone]").click(function () {

        var cloneItem = $(this).parents("[data-role=clone-block]").find("[data-role=clone-block-item]").not(':visible');

        cloneItem.find("input").attr("name", function (i, oldVal) {
            return oldVal.replace(/\[(\d+)\]/, function (_, m) {
                return "[" + (+m + 1) + "]";
            });
        }).attr("id", function (i, oldVal) {
            return oldVal.replace(/\-(\d+)/, function (_, m) {
                return "-" + (+m + 1);
            });
        });
        cloneItem.clone(true, true)
            .appendTo($(this).parents("[data-role=clone-block]")).show()
            .find("input");
        return false;
    });

    $("[data-role=clone-close]").click(function () {

        $(this).parent().parent().remove();
        return false;
    });
});
