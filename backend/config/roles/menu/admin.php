<?php
return [


    [
        'label' => \Yii::t('app.menu', 'Пользователи'),
        'url' => '#users',
        'icon' => 'users',
        'submenuId' => 'users',
        'items' => [
            [
                'label' => \Yii::t('app.menu', 'Пользователи'),
                'url' => ['/user/user/index'],
                'icon' => 'user',
            ],
            [
                'label' => \Yii::t('app.menu', 'Добавить пользователя'),
                'url' => ['/user/user/create'],
            ],

        ],
    ],
    [
        'label' => \Yii::t('app.menu', 'Local Bitcoin'),
        'url' => '#integrate',
        'icon' => 'catalog',
        'submenuId' => 'integrate',
        'items' => [
            [
                'label' => \Yii::t('app.menu', 'Добавить аккаунт'),
                'url' => ['/currency/currency/create'],
                'icon' => 'catalog',
            ],
            [
                'label' => \Yii::t('app.menu', 'Баланс'),
                'url' => ['/currency/currency/balance'],
                'icon' => 'catalog',
            ],
            [
                'label' => \Yii::t('app.menu', 'Курс'),
                'url' => ['/currency/currency/rate'],
                'icon' => 'catalog',
            ],
        ],
    ],
];
