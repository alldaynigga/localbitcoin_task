<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', \common\bootstraps\UserLoginBootstrap::class, 'systemLog'],
    'modules' => [
        'user' => [
            'class' => \backend\modules\user\Module::class,
        ],
        'catalog' => [
            'class' => backend\modules\catalog\Module::class,
        ],
        'currency' => [
            'class' => backend\modules\currency\Module::class,
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => common\models\User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'cookieParams' => ['lifetime' => 7 * 24 * 60 * 60],
            'timeout' => 3600 * 4,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => require __DIR__ . '/rules.php',
        ],
        'systemLog' => [
            'enabled' => true,
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => ['app.js'],
                    'sourcePath' => '@backend/web/js',
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],

            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
            'datetimeFormat' => 'php:d.m.Y в H:i:s',
            'timeFormat' => 'php:H:i:s',
            'locale' => 'ru-RU', //your language locale
            'defaultTimeZone' => 'Europe/Moscow', // time zone
        ],

    ],
    'container' => [
        'definitions' => [
            yii\grid\ActionColumn::class => [
                'template' => '<div class="table-action">{update} {delete}</div>',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return \yii\bootstrap\Html::a('<i class="align-middle mr-2 fas fa-fw fa-pencil-alt"></i>', $url,
                            [
                            ]);
                    },
                    'delete' => function ($url, $model) {
                        return \yii\bootstrap\Html::a('<i class="align-middle mr-2 fas fa-fw fa-trash"></i>', $url, [
                            'data-method' => 'post',
                            'data-confirm' => 'Вы действительно хотите удалить?',
                        ]);
                    },
                ],
            ],
            \yii\jui\DatePicker::class => [
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
            ],
            \yii\widgets\Breadcrumbs::class => [
                'itemTemplate' => "<li class=\"breadcrumb-item\">{link}</li>\n",
                'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                'tag' => 'ol',
            ],
        ],
    ],
    'params' => $params,
];
