<?php

namespace console\controllers;

use backend\modules\currency\infrastructure\repository\BalanceRepository;
use modules\import\common\components\sync\ItemSku\Synchronizator;
use yii\console\Controller;

/**
 * Class Rate
 * @package console\controllers
 */
class WalletController extends Controller
{

    /**
     * Initiate from cron one ti
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionBalance()
    {
        ini_set('memory_limit', '2048M');
        /** @type BalanceRepository $balanceRepository */
        $balanceRepository = \Yii::$container->get(BalanceRepository::class);
        $balanceRepository->load();
    }

}
