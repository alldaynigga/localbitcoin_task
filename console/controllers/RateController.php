<?php

namespace console\controllers;

use backend\modules\currency\app\client\request\BitStampBitFinexAvgMax;
use backend\modules\currency\app\client\request\Rate;
use backend\modules\currency\app\client\request\RateUsdInRub;
use backend\modules\currency\infrastructure\repository\BalanceRepository;
use backend\modules\currency\infrastructure\repository\RateRepository;
use modules\import\common\components\sync\ItemSku\Synchronizator;
use yii\console\Controller;

/**
 * Class Rate
 * @package console\controllers
 */
class RateController extends Controller
{

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(3600);
        /** @type RateRepository $rateRepository */
        $rateRepository = \Yii::$container->get(RateRepository::class);
        while (true) {
            foreach ($this->getRateRequestList() as $request) {
                $requestObject = \Yii::$container->get($request);
                $rateRepository->load($requestObject);
            }
            sleep(10);
        }
    }

    /**
     * @return array
     */
    protected function getRateRequestList()
    {
        return [
            BitStampBitFinexAvgMax::class,
            RateUsdInRub::class,
        ];
    }

}
