<?php

use yii\db\Migration;

/**
 * Class m190506_180444_create_core_user_tabels
 */
class m190506_180444_create_core_user_tabels extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%department}}',
            [
                'id' => $this->primaryKey(),
                'object_id' => $this->integer(11),
                'title' => $this->string(),
            ]
        );
        $this->addForeignKey('fk_department_object_id', 'department', 'object_id', 'object', 'object_id', 'RESTRICT',
            'RESTRICT');
        $this->createIndex('idx_department_object_id', 'department', 'object_id');

        $this->createTable(
            '{{%user_department}}',
            [
                'id' => $this->primaryKey(),
                'department_id' => $this->integer(11),
                'user_id' => $this->integer(11),
                'started_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
                'stopped_at' => $this->timestamp(),
                'status_id' => $this->integer(3)->notNull()->defaultValue(1),
            ]
        );
        $this->addForeignKey('fk_user_department_department_id', 'user_department', 'department_id', 'department', 'id',
            'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_user_department_department_id', 'user_department', 'department_id');
        $this->addForeignKey('fk_user_department_user_id', 'user_department', 'user_id', 'user', 'id', 'RESTRICT',
            'RESTRICT');
        $this->createIndex('idx_user_department_user_id', 'user_department', 'user_id');

        $this->createTable(
            '{{%browser}}',
            [
                'id' => $this->primaryKey(),
                'device_name' => $this->string(),
                'platform' => $this->string(),
                'useragent' => $this->string(),
                'browser_name' => $this->string(),
                'browser_version' => $this->string(),
            ]
        );
        $this->createTable(
            '{{%user_login}}',
            [
                'id' => $this->primaryKey(),
                'uid' => $this->string(32),
                'user_id' => $this->integer(11),
                'ip' => $this->string(32),
                'browser_id' => $this->integer(11),
                'created_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
                'expiry_at' => $this->timestamp(),
            ]
        );
        $this->addForeignKey('fk_user_login_user_id', 'user_login', 'user_id', 'user', 'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_user_login_user_id', 'user_login', 'user_id');
        $this->addForeignKey('fk_user_login_browser_id', 'user_login', 'browser_id', 'browser', 'id', 'RESTRICT',
            'RESTRICT');
        $this->createIndex('idx_user_login_browser_id', 'user_login', 'browser_id');
        $this->createIndex('uidx_user_login_uid', 'user_login', 'uid', true);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190506_180444_create_core_user_tabels cannot be reverted.\n";

        return false;
    }
}
