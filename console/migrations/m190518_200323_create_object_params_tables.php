<?php

use yii\db\Migration;

/**
 * Class m190518_200323_create_object_params_tables
 */
class m190518_200323_create_object_params_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('enum_object_params', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'title' => $this->string(),
            'description' => $this->string(),
            'type_id' => $this->integer(2),
            'class_namespace' => $this->string(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
        ]);

        $this->createTable('object_params', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(11),
            'enum_id' => $this->integer(11),
            'value_int' => $this->integer(),
            'value_float' => $this->double(),
            'value_varchar' => $this->string(255),
            'value_text' => $this->text(),
            'value_json' => $this->text(),
        ]);

        $this->addForeignKey('fk_object_params_object_id', 'object_params', 'object_id', 'object', 'object_id',
            'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_object_params_object_id', 'object_params', 'object_id');
        $this->addForeignKey('fk_object_params_enum_id', 'object_params', 'enum_id', 'enum_object_params', 'id',
            'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_object_params_enum_id', 'object_params', 'enum_id');

        $this->insert('enum_object_params', [
            'type_id' => \common\models\EnumObjectParams::TYPE_JSON,
            'name' => 'emails',
            'title' => 'Дополнительный  Email',
        ]);

        $this->insert('enum_object_params', [
            'type_id' => \common\models\EnumObjectParams::TYPE_JSON,
            'name' => 'phones',
            'title' => 'Дополнительный телефон',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190518_200323_create_object_params_tables cannot be reverted.\n";

        return false;
    }
}
