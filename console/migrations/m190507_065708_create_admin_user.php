<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m190507_065708_create_admin_user
 */
class m190507_065708_create_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('object_vocabulary', [
            'class_namespace' => User::class,
            'name' => 'user',
            'table_name' => 'user',
        ]);
        $this->insert('object', [
            'object_vocabulary_id' => \common\models\ObjectVocabulary::getIdByClassNamespace(\common\models\User::class),
        ]);

        $objectId = $this->getDb()->createCommand('SELECT object_id FROM object ORDER BY object_id DESC LIMIT 1')->queryScalar();

        $this->insert('user', [
            'object_id' => $objectId,
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'status_id' => User::STATUS_ACTIVE,
            'auth_key' => Yii::$app->security->generateRandomString(),
        ]);
        $userId = Yii::$app->db->getLastInsertID();

        $authManager = Yii::createObject(\common\components\rbac\OSDBManager::class);
        /**
         * Роль администратор
         */
        $adminAccess = $authManager->createPermission('adminAccess');
        $adminAccess->description = 'Доступ к администрации';
        $authManager->add($adminAccess);

        $adminUse = $authManager->createPermission('adminUse');
        $adminUse->description = 'Доступ к модулям администрации';
        $authManager->add($adminUse);

        $adminRole = $authManager->createRole('admin');
        $adminRole->description = 'Администратор системы';
        $authManager->add($adminRole);
        $authManager->addChild($adminRole, $adminAccess);
        $authManager->addChild($adminRole, $adminUse);
        $authManager->assign($adminRole, $userId);

        /**
         * Роль Директор
         */
        $headmasterAccess = $authManager->createPermission('headmasterAccess');
        $headmasterAccess->description = 'Доступ к кабинету';
        $authManager->add($headmasterAccess);

        $headmasterUse = $authManager->createPermission('headmasterUse');
        $headmasterUse->description = 'Доступ к модулям';
        $authManager->add($headmasterUse);

        $headmasterRole = $authManager->createRole('headmaster');
        $headmasterRole->description = 'Директор';
        $authManager->add($headmasterRole);
        $authManager->addChild($headmasterRole, $headmasterAccess);
        $authManager->addChild($headmasterRole, $headmasterUse);

        /**
         * Роль Финансовый директор
         */
        $financedirectorAccess = $authManager->createPermission('financedirectorAccess');
        $financedirectorAccess->description = 'Доступ к кабинету';
        $authManager->add($financedirectorAccess);

        $financedirectorUse = $authManager->createPermission('financedirectorUse');
        $financedirectorUse->description = 'Доступ к модулям';
        $authManager->add($financedirectorUse);

        $financedirectorRole = $authManager->createRole('financedirector');
        $financedirectorRole->description = 'Финансовый директор';
        $authManager->add($financedirectorRole);
        $authManager->addChild($financedirectorRole, $financedirectorAccess);
        $authManager->addChild($financedirectorRole, $financedirectorUse);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190507_065708_create_admin_user cannot be reverted.\n";

        return false;
    }

}
