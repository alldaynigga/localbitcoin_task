<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%driver}}`.
 */
class m190526_124506_add_bitcoin_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rate}}', [
            'id' => $this->primaryKey(),
            'amount' => $this->double(15),
            'formula' => $this->string(255),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
        ]);

        $this->createTable('{{%balance}}', [
            'id' => $this->primaryKey(),
            'amount' => $this->double(15),
            'account_id' => $this->integer(11),
            'created_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
        ]);

        $this->createTable('{{%account}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(80)->unique()->notNull(),
            'api_key' => $this->string(255)->notNull(),
            'secret_key' => $this->string(255)->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
        ]);

        $this->addForeignKey('fk_balance_account', '{{%balance}}', 'account_id', '{{%account}}', 'id', 'SET NULL',
            'SET NULL');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
