<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%object_vocabulary}}', [
            'id' => $this->primaryKey(),
            'class_namespace' => $this->string()->notNull(),
            'name' => $this->string(64)->notNull(),
            'table_name' => $this->string(64)->notNull(),
            'description' => $this->text(),
        ]);
        $this->createIndex('undx_object_vocabulary_name', 'object_vocabulary', 'name', true);
        $this->createIndex('undx_object_vocabulary_table_name', 'object_vocabulary', 'table_name', true);

        $this->createTable('{{%object}}', [
            'object_id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP'))->comment('Дата создания'),
            'created_by' => $this->integer(11),
            'updated_at' => $this->timestamp()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP'))->comment('Последнее изменение'),
            'updated_by' => $this->integer(11),
            'is_deleted' => $this->integer(1)->defaultValue(0),
            'object_vocabulary_id' => $this->integer(11),
        ]);

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'status_id' => $this->integer(3)->notNull()->defaultValue(10),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'middle_name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'object_id' => $this->integer(11),
            'blocked_at' => $this->timestamp(),

        ]);

        $this->addForeignKey('fk_object_created_by', 'object', 'created_by', 'user', 'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_object_created_by', 'object', 'created_by');

        $this->addForeignKey('fk_object_updated_by', 'object', 'updated_by', 'user', 'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_object_updated_by', 'object', 'updated_by');

        $this->addForeignKey('fk_object_object_vocabulary_id', 'object', 'object_vocabulary_id', 'object_vocabulary',
            'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_object_object_vocabulary_id', 'object', 'object_vocabulary_id');

        $this->addForeignKey('fk_user_object_id', 'user', 'object_id', 'object', 'object_id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_user_object_id', 'user', 'object_id');


        return true;
    }

    public function down()
    {
        return false;
    }
}
