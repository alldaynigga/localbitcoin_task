<?php

use yii\db\Migration;

/**
 * Class m190509_143928_create_system_log_tables
 */
class m190509_143928_create_system_log_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('system_log', [
            'id' => $this->primaryKey(),
            'user_login_id' => $this->integer(),
            'started_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
            'stopped_at' => $this->timestamp()->notNull()->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP')),
            'url' => $this->string(1024),
            'params' => $this->text(),
            'response_status' => $this->integer(),
            'error_message' => $this->text(),
        ]);

        $this->addForeignKey('fk_system_log_user_login', 'system_log', 'user_login_id', 'user_login', 'id', 'RESTRICT',
            'RESTRICT');
        $this->createIndex('idx_system_log_user_login', 'system_log', 'user_login_id');

        $this->createTable('system_log_detail', [
            'id' => $this->primaryKey(),
            'system_log_id' => $this->integer()->notNull(),
            'class_namespace' => $this->string(),
            'changed_attributes' => $this->text(),
            'is_saved' => $this->integer(1),
            'transaction_level' => $this->integer(),
            'transaction_status' => $this->integer(3),
        ]);

        $this->addForeignKey('fk_system_log_detail_system_log', 'system_log_detail', 'system_log_id', 'system_log',
            'id', 'RESTRICT', 'RESTRICT');
        $this->createIndex('idx_system_log_detail_system_log', 'system_log_detail', 'system_log_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190509_143928_create_system_log_tables cannot be reverted.\n";

        return false;
    }
}
