<?php

use yii\db\Migration;

/**
 * Class m190525_120115_add_roles
 */
class m190525_120115_add_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $authManager = Yii::createObject(\common\components\rbac\OSDBManager::class);

        /**
         * Роль сотрудник отдела
         */
        $employeeAccess = $authManager->createPermission('employeeAccess');
        $employeeAccess->description = 'Доступ к кабинету сотрудника';
        $authManager->add($employeeAccess);

        $employeeUse = $authManager->createPermission('employeeUse');
        $employeeUse->description = 'Доступ к модулям сотрудника';
        $authManager->add($employeeUse);

        $employeeRole = $authManager->createRole('employee');
        $employeeRole->description = 'Сотрудник';
        $authManager->add($employeeRole);
        $authManager->addChild($employeeRole, $employeeAccess);
        $authManager->addChild($employeeRole, $employeeUse);

        /**
         * Роль начальник отдела
         */
        $managerAccess = $authManager->createPermission('managerAccess');
        $managerAccess->description = 'Доступ к кабинету начальника отдела';
        $authManager->add($managerAccess);

        $managerUse = $authManager->createPermission('managerUse');
        $managerUse->description = 'Доступ к модулям начальника отдела';
        $authManager->add($managerUse);

        $managerRole = $authManager->createRole('manager');
        $managerRole->description = 'Начальник отдела';
        $authManager->add($managerRole);
        $authManager->addChild($managerRole, $managerAccess);
        $authManager->addChild($managerRole, $managerUse);

        /**
         * Роль сотрудник отдела развития
         */
        $employeeEvolutionAccess = $authManager->createPermission('employeeEvolutionAccess');
        $employeeEvolutionAccess->description = 'Доступ к кабинету сотрудника отдела развития';
        $authManager->add($employeeEvolutionAccess);

        $employeeEvolutionUse = $authManager->createPermission('employeeEvolutionUse');
        $employeeEvolutionUse->description = 'Доступ к модулям сотрудника отдела развития';
        $authManager->add($employeeEvolutionUse);

        $employeeEvolutionRole = $authManager->createRole('employeeEvolution');
        $employeeEvolutionRole->description = 'Сотрудник отдела развития';
        $authManager->add($employeeEvolutionRole);
        $authManager->addChild($employeeEvolutionRole, $employeeRole);
        $authManager->addChild($employeeEvolutionRole, $employeeEvolutionAccess);
        $authManager->addChild($employeeEvolutionRole, $employeeEvolutionUse);

        /**
         * Роль начальник отдела развития
         */
        $managerEvolutionAccess = $authManager->createPermission('managerEvolutionAccess');
        $managerEvolutionAccess->description = 'Доступ к кабинету начальника отдела развития';
        $authManager->add($managerEvolutionAccess);

        $managerEvolutionUse = $authManager->createPermission('managerEvolutionUse');
        $managerEvolutionUse->description = 'Доступ к модулям начальника отдела развития';
        $authManager->add($managerEvolutionUse);

        $managerEvolutionRole = $authManager->createRole('managerEvolution');
        $managerEvolutionRole->description = 'Начальник отдела развития';
        $authManager->add($managerEvolutionRole);
        $authManager->addChild($managerEvolutionRole, $managerRole);
        $authManager->addChild($managerEvolutionRole, $managerEvolutionAccess);
        $authManager->addChild($managerEvolutionRole, $managerEvolutionUse);

        /**
         * Роль сотрудник отдела продаж
         */
        $employeeRealizationAccess = $authManager->createPermission('employeeRealizationAccess');
        $employeeRealizationAccess->description = 'Доступ к кабинету сотрудника отдела продаж';
        $authManager->add($employeeRealizationAccess);

        $employeeRealizationUse = $authManager->createPermission('employeeRealizationUse');
        $employeeRealizationUse->description = 'Доступ к модулям сотрудника отдела продаж';
        $authManager->add($employeeRealizationUse);

        $employeeRealizationRole = $authManager->createRole('employeeRealization');
        $employeeRealizationRole->description = 'Сотрудник отдела продаж';
        $authManager->add($employeeRealizationRole);
        $authManager->addChild($employeeRealizationRole, $employeeRole);
        $authManager->addChild($employeeRealizationRole, $employeeRealizationAccess);
        $authManager->addChild($employeeRealizationRole, $employeeRealizationUse);

        /**
         * Роль начальник отдела продаж
         */
        $managerRealizationAccess = $authManager->createPermission('managerRealizationAccess');
        $managerRealizationAccess->description = 'Доступ к кабинету начальника отдела продаж';
        $authManager->add($managerRealizationAccess);

        $managerRealizationUse = $authManager->createPermission('managerRealizationUse');
        $managerRealizationUse->description = 'Доступ к модулям начальника отдела продаж';
        $authManager->add($managerRealizationUse);

        $managerRealizationRole = $authManager->createRole('managerRealization');
        $managerRealizationRole->description = 'Начальник отдела продаж';
        $authManager->add($managerRealizationRole);
        $authManager->addChild($managerRealizationRole, $managerRole);
        $authManager->addChild($managerRealizationRole, $managerRealizationAccess);
        $authManager->addChild($managerRealizationRole, $managerRealizationUse);

        /**
         * Роль сотрудник отдела производства
         */
        $employeeProductionAccess = $authManager->createPermission('employeeProductionAccess');
        $employeeProductionAccess->description = 'Доступ к кабинету сотрудника отдела производства';
        $authManager->add($employeeProductionAccess);

        $employeeProductionUse = $authManager->createPermission('employeeProductionUse');
        $employeeProductionUse->description = 'Доступ к модулям сотрудника отдела производства';
        $authManager->add($employeeProductionUse);

        $employeeProductionRole = $authManager->createRole('employeeProduction');
        $employeeProductionRole->description = 'Сотрудник';
        $authManager->add($employeeProductionRole);
        $authManager->addChild($employeeProductionRole, $employeeRole);
        $authManager->addChild($employeeProductionRole, $employeeProductionAccess);
        $authManager->addChild($employeeProductionRole, $employeeProductionUse);

        /**
         * Роль начальник отдела развития
         */
        $managerProductionAccess = $authManager->createPermission('managerProductionAccess');
        $managerProductionAccess->description = 'Доступ к кабинету начальника отдела производства';
        $authManager->add($managerProductionAccess);

        $managerProductionUse = $authManager->createPermission('managerProductionUse');
        $managerProductionUse->description = 'Доступ к модулям начальника отдела производства';
        $authManager->add($managerProductionUse);

        $managerProductionRole = $authManager->createRole('managerProduction');
        $managerProductionRole->description = 'Начальник отдела производства';
        $authManager->add($managerProductionRole);
        $authManager->addChild($managerProductionRole, $managerRole);
        $authManager->addChild($managerProductionRole, $managerProductionAccess);
        $authManager->addChild($managerProductionRole, $managerProductionUse);

        /**
         * Роль сотрудник отдела импорта
         */
        $employeeImportAccess = $authManager->createPermission('employeeImportAccess');
        $employeeImportAccess->description = 'Доступ к кабинету сотрудника отдела импорта';
        $authManager->add($employeeImportAccess);

        $employeeImportUse = $authManager->createPermission('employeeImportUse');
        $employeeImportUse->description = 'Доступ к модулям сотрудника отдела импорта';
        $authManager->add($employeeImportUse);

        $employeeImportRole = $authManager->createRole('employeeImport');
        $employeeImportRole->description = 'Сотрудник отдела импорта';
        $authManager->add($employeeImportRole);
        $authManager->addChild($employeeImportRole, $employeeRole);
        $authManager->addChild($employeeImportRole, $employeeImportAccess);
        $authManager->addChild($employeeImportRole, $employeeImportUse);

        /**
         * Роль начальник отдела импорта
         */
        $managerImportAccess = $authManager->createPermission('managerImportAccess');
        $managerImportAccess->description = 'Доступ к кабинету начальника отдела импорта';
        $authManager->add($managerImportAccess);

        $managerImportUse = $authManager->createPermission('managerImportUse');
        $managerImportUse->description = 'Доступ к модулям начальника отдела импорта';
        $authManager->add($managerImportUse);

        $managerImportRole = $authManager->createRole('managerImport');
        $managerImportRole->description = 'Начальник отдела импорта';
        $authManager->add($managerImportRole);
        $authManager->addChild($managerImportRole, $managerRole);
        $authManager->addChild($managerImportRole, $managerImportAccess);
        $authManager->addChild($managerImportRole, $managerImportUse);

        /**
         * Роль сотрудник отдела сертификации
         */
        $employeeCertificationAccess = $authManager->createPermission('employeeCertificationAccess');
        $employeeCertificationAccess->description = 'Доступ к кабинету сотрудника отдела сертификации';
        $authManager->add($employeeCertificationAccess);

        $employeeCertificationUse = $authManager->createPermission('employeeCertificationUse');
        $employeeCertificationUse->description = 'Доступ к модулям сотрудника отдела сертификации';
        $authManager->add($employeeCertificationUse);

        $employeeCertificationRole = $authManager->createRole('employeeCertification');
        $employeeCertificationRole->description = 'Сотрудник отдела сертификации';
        $authManager->add($employeeCertificationRole);
        $authManager->addChild($employeeCertificationRole, $employeeRole);
        $authManager->addChild($employeeCertificationRole, $employeeCertificationAccess);
        $authManager->addChild($employeeCertificationRole, $employeeCertificationUse);

        /**
         * Роль начальник отдела сертификации
         */
        $managerCertificationAccess = $authManager->createPermission('managerCertificationAccess');
        $managerCertificationAccess->description = 'Доступ к кабинету начальника отдела сертификации';
        $authManager->add($managerCertificationAccess);

        $managerCertificationUse = $authManager->createPermission('managerCertificationUse');
        $managerCertificationUse->description = 'Доступ к модулям начальника отдела сертификации';
        $authManager->add($managerCertificationUse);

        $managerCertificationRole = $authManager->createRole('managerCertification');
        $managerCertificationRole->description = 'Начальник отдела сертификации';
        $authManager->add($managerCertificationRole);
        $authManager->addChild($managerCertificationRole, $managerRole);
        $authManager->addChild($managerCertificationRole, $managerCertificationAccess);
        $authManager->addChild($managerCertificationRole, $managerCertificationUse);

        /**
         * Роль сотрудник отдела сертификации
         */
        $employeeDesignAccess = $authManager->createPermission('employeeDesignAccess');
        $employeeDesignAccess->description = 'Доступ к кабинету сотрудника отдела дизайна';
        $authManager->add($employeeDesignAccess);

        $employeeDesignUse = $authManager->createPermission('employeeDesignUse');
        $employeeDesignUse->description = 'Доступ к модулям сотрудника отдела дизайна';
        $authManager->add($employeeDesignUse);

        $employeeDesignRole = $authManager->createRole('employeeDesign');
        $employeeDesignRole->description = 'Сотрудник отдела дизайна';
        $authManager->add($employeeDesignRole);
        $authManager->addChild($employeeDesignRole, $employeeRole);
        $authManager->addChild($employeeDesignRole, $employeeDesignAccess);
        $authManager->addChild($employeeDesignRole, $employeeDesignUse);

        /**
         * Роль начальник отдела сертификации
         */
        $managerDesignAccess = $authManager->createPermission('managerDesignAccess');
        $managerDesignAccess->description = 'Доступ к кабинету начальника отдела дизайна';
        $authManager->add($managerDesignAccess);

        $managerDesignUse = $authManager->createPermission('managerDesignUse');
        $managerDesignUse->description = 'Доступ к модулям начальника отдела дизайна';
        $authManager->add($managerDesignUse);

        $managerDesignRole = $authManager->createRole('managerDesign');
        $managerDesignRole->description = 'Начальник отдела дизайна';
        $authManager->add($managerDesignRole);
        $authManager->addChild($managerDesignRole, $managerRole);
        $authManager->addChild($managerDesignRole, $managerDesignAccess);
        $authManager->addChild($managerDesignRole, $managerDesignUse);

        /**
         * Роль сотрудник отдела логистики
         */
        $employeeLogisticsAccess = $authManager->createPermission('employeeLogisticsAccess');
        $employeeLogisticsAccess->description = 'Доступ к кабинету сотрудника отдела логистики';
        $authManager->add($employeeLogisticsAccess);

        $employeeLogisticsUse = $authManager->createPermission('employeeLogisticsUse');
        $employeeLogisticsUse->description = 'Доступ к модулям сотрудника отдела логистики';
        $authManager->add($employeeLogisticsUse);

        $employeeLogisticsRole = $authManager->createRole('employeeLogistics');
        $employeeLogisticsRole->description = 'Сотрудник отдела логистики';
        $authManager->add($employeeLogisticsRole);
        $authManager->addChild($employeeLogisticsRole, $employeeRole);
        $authManager->addChild($employeeLogisticsRole, $employeeLogisticsAccess);
        $authManager->addChild($employeeLogisticsRole, $employeeLogisticsUse);

        /**
         * Роль начальник отдела сертификации
         */
        $managerLogisticsAccess = $authManager->createPermission('managerLogisticsAccess');
        $managerLogisticsAccess->description = 'Доступ к кабинету начальника отдела логистики';
        $authManager->add($managerLogisticsAccess);

        $managerLogisticsUse = $authManager->createPermission('managerLogisticsUse');
        $managerLogisticsUse->description = 'Доступ к модулям начальника отдела логистики';
        $authManager->add($managerLogisticsUse);

        $managerLogisticsRole = $authManager->createRole('managerLogistics');
        $managerLogisticsRole->description = 'Начальник отдела логистики';
        $authManager->add($managerLogisticsRole);
        $authManager->addChild($managerLogisticsRole, $managerRole);
        $authManager->addChild($managerLogisticsRole, $managerLogisticsAccess);
        $authManager->addChild($managerLogisticsRole, $managerLogisticsUse);

        /**
         * Роль сотрудник отдела склада
         */
        $employeeStorehouseAccess = $authManager->createPermission('employeeStorehouseAccess');
        $employeeStorehouseAccess->description = 'Доступ к кабинету сотрудника отдела склада';
        $authManager->add($employeeStorehouseAccess);

        $employeeStorehouseUse = $authManager->createPermission('employeeStorehouseUse');
        $employeeStorehouseUse->description = 'Доступ к модулям сотрудника отдела склада';
        $authManager->add($employeeStorehouseUse);

        $employeeStorehouseRole = $authManager->createRole('employeeStorehouse');
        $employeeStorehouseRole->description = 'Сотрудник отдела склада';
        $authManager->add($employeeStorehouseRole);
        $authManager->addChild($employeeStorehouseRole, $employeeRole);
        $authManager->addChild($employeeStorehouseRole, $employeeStorehouseAccess);
        $authManager->addChild($employeeStorehouseRole, $employeeStorehouseUse);

        /**
         * Роль начальник отдела сертификации
         */
        $managerStorehouseAccess = $authManager->createPermission('managerStorehouseAccess');
        $managerStorehouseAccess->description = 'Доступ к кабинету начальника отдела склада';
        $authManager->add($managerStorehouseAccess);

        $managerStorehouseUse = $authManager->createPermission('managerStorehouseUse');
        $managerStorehouseUse->description = 'Доступ к модулям начальника отдела склада';
        $authManager->add($managerStorehouseUse);

        $managerStorehouseRole = $authManager->createRole('managerStorehouse');
        $managerStorehouseRole->description = 'Начальник отдела склада';
        $authManager->add($managerStorehouseRole);
        $authManager->addChild($managerStorehouseRole, $managerRole);
        $authManager->addChild($managerStorehouseRole, $managerStorehouseAccess);
        $authManager->addChild($managerStorehouseRole, $managerStorehouseUse);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190525_120115_add_roles cannot be reverted.\n";

        return false;
    }
}
