<?php

namespace common\bootstraps;

use common\models\User;
use yii\base\BootstrapInterface;

class UserLoginBootstrap implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        $app->user->on(\yii\web\User::EVENT_AFTER_LOGIN, [User::class, 'afterLogin']);
    }
}