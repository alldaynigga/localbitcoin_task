<?php

namespace common\widgets;

use rmrevin\yii\fontawesome\component\Icon;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\Widget;

class BlockInputWidget extends Widget
{
    public $form;
    public $model;
    public $attribute;
    public $arrayAttribute;

    public function run()
    {
        parent::run();

        return $this->render('block-input', [
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'arrayAttribute' => $this->arrayAttribute,
        ]);
    }
}
