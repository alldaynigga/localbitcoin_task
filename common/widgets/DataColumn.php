<?php

namespace common\widgets;

use yii\grid\DataColumn as BaseColumn;
use yii\helpers\Html;

class DataColumn extends BaseColumn
{
    public $headerOptions = ['class' => 'sorting'];

    public $filter = false;

    /**
     * {@inheritdoc}
     */
    protected function renderHeaderCellContent()
    {
        if ($this->header !== null || $this->label === null && $this->attribute === null) {
            return parent::renderHeaderCellContent();
        }

        $label = $this->getHeaderCellLabel();
        if ($this->encodeLabel) {
            $label = Html::encode($label);
        }

        if ($this->attribute !== null && $this->enableSorting &&
            ($sort = $this->grid->dataProvider->getSort()) !== false && $sort->hasAttribute($this->attribute)) {
            return $sort->link($this->attribute, array_merge($this->sortLinkOptions, ['label' => $label]));
        }

        return $label;
    }

    /**
     * Renders the header cell.
     */
    public function renderHeaderCell()
    {
        if ($this->attribute !== null && $this->enableSorting &&
            ($sort = $this->grid->dataProvider->getSort()) !== false && $sort->hasAttribute($this->attribute)) {
            if (($direction = $sort->getAttributeOrder($this->attribute)) !== null) {
                $class = $direction === SORT_DESC ? 'sorting_desc' : 'sorting_asc';
                if (isset($this->headerOptions['class'])) {
                    $this->headerOptions['class'] .= ' ' . $class;
                } else {
                    $this->headerOptions['class'] = $class;
                }
            }
        }

        return Html::tag('th', $this->renderHeaderCellContent(), $this->headerOptions);
    }
}
