<?php

namespace common\widgets;

use yii\grid\GridView as BaseGridView;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;

class GridView extends BaseGridView
{
    public $layout = "<div class=\"row\"><div class=\"col-sm-12\">{items}\n</div></div><div class=\"row\"><div class=\"col-sm-12 col-md-5\">{summary}</div><div class=\"col-sm-12 col-md-7\"><div class=\"dataTables_paginate paging_simple_numbers\">{pager}\n</div></div></div>";
    public $tableOptions = ['class' => 'table table-striped dataTable dtr-inline'];
    public $headerOptions = ['class' => 'sorting_asc'];
    public $dataColumnClass = DataColumn::class;
    public $summaryOptions = ['class' => 'dataTables_info'];

    /**
     * Runs the widget.
     */
    public function run()
    {
        $view = $this->getView();
        $view->registerJs('$("th").click(function(){
        if($(this).children("a").length){
        $(this).children("a")[0].click();}});');
        parent::run();
    }

    /**
     * Renders the pager.
     * @return string the rendering result
     */
    public function renderPager()
    {
        $pagination = $this->dataProvider->getPagination();
        if ($pagination === false || $this->dataProvider->getCount() <= 0) {
            return '';
        }
        /* @var $class LinkPager */
        $pager = $this->pager;
        $class = ArrayHelper::remove($pager, 'class', LinkPager::className());
        $pager['pagination'] = $pagination;
        $pager['view'] = $this->getView();
        $pager['firstPageCssClass'] = 'paginate_button page-item previous';
        $pager['prevPageCssClass'] = 'paginate_button page-item';
        $pager['pageCssClass'] = 'paginate_button page-item';
        $pager['linkOptions'] = ['class' => 'page-link'];
        $pager['disabledListItemSubTagOptions'] = ['tag' => 'a', 'class' => 'page-link'];

        return $class::widget($pager);
    }

    /**
     * Renders the filter.
     * @return string the rendering result.
     */
    public function renderFilters()
    {
        return '';
    }
}
