<?php

namespace common\widgets;

use yii\bootstrap\ActiveForm as BaseActiveForm;

class ActiveForm extends BaseActiveForm
{
    public $fieldClass = ActiveField::class;

    public $validationStateOn = BaseActiveForm::VALIDATION_STATE_ON_INPUT;

    public $errorCssClass = 'is-invalid';

    public $layout = 'horizontal';
}
