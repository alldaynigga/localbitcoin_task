<div data-role="clone-block">
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, $attribute)->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <button class="btn" data-role="clone"><i class="align-middle" data-feather="plus-circle"></i></button>
        </div>
    </div>
    <div class="row" data-role="clone-block-item" style="display: none">
        <div class="col-md-8">
            <?= $form->field($model,
                $arrayAttribute . '[' . (is_array($model->$arrayAttribute) ? count($model->$arrayAttribute) : 0) . ']')->textInput([
                'maxlength' => true,
                'value' => '',
            ]) ?>
        </div>
        <div class="col-md-4">
            <button class="btn" data-role="clone-close"><i class="align-middle" data-feather="minus-circle"></i>
            </button>
        </div>
    </div>
    <?php if ($model->$arrayAttribute && is_array($model->$arrayAttribute)) {
        foreach ($model->$arrayAttribute as $key => $item) {
            if ($item) {
                ?>
                <div class="row" data-role="clone-block-item">
                    <div class="col-md-8">
                        <?= $form->field($model, $arrayAttribute . '[' . $key . ']')->textInput([
                            'maxlength' => true,
                            'value' => $item,
                        ]) ?>
                    </div>
                    <div class="col-md-4">
                        <button class="btn" data-role="clone-close"><i class="align-middle"
                                                                       data-feather="minus-circle"></i></button>
                    </div>
                </div>
            <?php }
        }
    } else {
        $key = 0;
    } ?>
</div>
