<div data-role="clone-block">
    <div class="form-row">
        <?= $form->field($model, $attribute,
            ['options' => ['class' => 'form-group col-md-6']])->textInput(['maxlength' => true]) ?>
        <button class="btn" data-role="clone"><i class="align-middle" data-feather="plus-circle"></i></button>
    </div>
    <div class="form-row" data-role="clone-block-item" style="display: none">
        <?= $form->field($model,
            $arrayAttribute . '[' . (is_array($model->$arrayAttribute) ? count($model->$arrayAttribute) : 0) . ']',
            ['options' => ['class' => 'form-group col-md-6']])->textInput(['maxlength' => true, 'value' => '']) ?>
        <button class="btn" data-role="clone-close"><i class="align-middle" data-feather="minus-circle"></i></button>
    </div>
    <?php if ($model->$arrayAttribute && is_array($model->$arrayAttribute)) {
        foreach ($model->$arrayAttribute as $key => $item) {
            if ($item) {
                ?>
                <div class="form-row" data-role="clone-block-item">
                    <?= $form->field($model, $arrayAttribute . '[' . $key . ']',
                        ['options' => ['class' => 'form-group col-md-6']])->textInput([
                        'maxlength' => true,
                        'value' => $item,
                    ]) ?>
                    <button class="btn" data-role="clone-close"><i class="align-middle" data-feather="minus-circle"></i>
                    </button>
                </div>
            <?php }
        }
    } else {
        $key = 0;
    } ?>
</div>
