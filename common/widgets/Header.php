<?php

namespace common\widgets;

use yii\bootstrap\Widget;

class Header extends Widget
{
    public function run()
    {
        parent::run();

        return $this->render('header');
    }
}
