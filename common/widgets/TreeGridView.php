<?php

namespace common\widgets;

use leandrogehlen\treegrid\TreeGrid as BaseGridView;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;

class TreeGridView extends BaseGridView
{
    public $layout = "<div class=\"row\"><div class=\"col-sm-12\">{items}\n</div></div><div class=\"row\"><div class=\"col-sm-12 col-md-5\">{summary}</div><div class=\"col-sm-12 col-md-7\"><div class=\"dataTables_paginate paging_simple_numbers\">{pager}\n</div></div></div>";
    public $options = ['class' => 'table table-striped dataTable dtr-inline', 'id' => 'item-tree'];
    public $headerOptions = ['class' => 'sorting_asc'];
    public $dataColumnClass = DataColumn::class;
    public $summaryOptions = ['class' => 'dataTables_info'];
    public $bordered = false;
    public $pluginOptions = ['initialState' => 'collapsed'];

    /**
     * Runs the widget.
     * @todo переписать js код когда определимся с функционалом
     */
    public function run()
    {
        $view = $this->getView();
        $view->registerJs('$("th").click(function(){
        if($(this).children("a").length){
        $(this).children("a")[0].click();}});
        $("#item-tree").one("expand", function(){
            $(".treegrid-indent").parent().next().prepend(\'<span class="treegrid-indent"></span>\');
        });
        $("#item-tree").one("expand", function(){
            $(".treegrid-indent + .treegrid-indent").parent().next().prepend(\'<span class="treegrid-indent"></span>\');
        });
        ');

        return parent::run();
    }
}
