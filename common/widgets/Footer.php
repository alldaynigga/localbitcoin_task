<?php

namespace common\widgets;

use yii\bootstrap\Widget;

class Footer extends Widget
{
    public function run()
    {
        parent::run();

        return $this->render('footer');
    }
}
