<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_department".
 *
 * @property int $id
 * @property int $department_id
 * @property int $user_id
 * @property string $started_at
 * @property string $stopped_at
 * @property int $status_id
 *
 * @property Department $department
 * @property User $user
 */
class UserDepartment extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['department_id', 'user_id'], 'default', 'value' => null],
            [['department_id', 'user_id', 'status_id'], 'integer'],
            [['started_at', 'stopped_at'], 'safe'],
            [['status_id'], 'required'],
            [
                ['department_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Department::class,
                'targetAttribute' => ['department_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_id' => Yii::t('app', 'Department ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'started_at' => Yii::t('app', 'Started At'),
            'stopped_at' => Yii::t('app', 'Stopped At'),
            'status_id' => Yii::t('app', 'Status ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\UserDepartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\UserDepartmentQuery(get_called_class());
    }
}
