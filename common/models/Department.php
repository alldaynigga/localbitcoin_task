<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "department".
 *
 * @property int $id
 * @property int $object_id
 * @property string $title
 *
 * @property Object $object
 * @property UserDepartment[] $userDepartments
 */
class Department extends ObjectedModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_id'], 'default', 'value' => null],
            [['object_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [
                ['object_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Object::class,
                'targetAttribute' => ['object_id' => 'object_id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Object::class, ['object_id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDepartments()
    {
        return $this->hasMany(UserDepartment::class, ['department_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\DepartmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\DepartmentQuery(get_called_class());
    }
}
