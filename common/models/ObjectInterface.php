<?php

namespace common\models;

interface ObjectInterface
{
    public function getObjectId(): ?int;

    public function getCurrentUserId(): ?int;
}