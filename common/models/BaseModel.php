<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the base model class
 *
 */
class BaseModel extends \yii\db\ActiveRecord
{
    public const SCENARIO_CREATE = 'create';
    /**
     * Указываются переменные значения которых не должны быть в логе
     *
     * @return array
     */
    public $withoutLogging = [

    ];

    /**
     * Получаем измененные поля до сохранения, так как после сохранения они могут быть некорректны
     * {@inheritdoc}
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (Yii::$app->systemLog->enabled) {
            $dirtyAttributes = $this->getDirtyAttributesForLog();
            if ($this->getIsNewRecord()) {
                $savedStatus = $this->insert($runValidation, $attributeNames);
            } else {
                $savedStatus = $this->update($runValidation, $attributeNames) !== false;
            }
            if (isset($this->id)) {
                $dirtyAttributes = ArrayHelper::merge($dirtyAttributes, ['id' => $this->id]);
            }
            Yii::$app->systemLog->createSystemLogDetail(static::class, $dirtyAttributes, $savedStatus);

            return $savedStatus;
        }

        return parent::save($runValidation, $attributeNames);
    }

    /**
     * Возвращает измененные поля, которые должны быть прологированы
     *
     * @return array
     */
    public function getDirtyAttributesForLog(): array
    {
        $names = $this->attributes();
        $names = array_diff($names, $this->withoutLogging);
        $names = array_flip($names);
        $attributes = [];
        $oldAttributes = $this->getOldAttributes();
        $attributesValues = $this->getAttributes();
        if (!$oldAttributes) {
            foreach ($attributesValues as $name => $value) {
                if (isset($names[$name])) {
                    $attributes[$name] = $value;
                }
            }
        } else {
            foreach ($attributesValues as $name => $value) {
                if (isset($names[$name]) && (!array_key_exists($name,
                            $oldAttributes) || $value != $oldAttributes[$name])) {
                    $attributes[$name] = $value;
                }
            }
        }

        return $attributes;
    }
}
