<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_login".
 *
 * @property int $id
 * @property string $uid
 * @property int $user_id
 * @property string $ip
 * @property int $browser_id
 * @property string $created_at
 * @property string $expiry_at
 *
 * @property Browser $browser
 * @property User $user
 */
class UserLogin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'browser_id'], 'default', 'value' => null],
            [['user_id', 'browser_id'], 'integer'],
            [['created_at', 'expiry_at'], 'safe'],
            [['uid', 'ip'], 'string', 'max' => 32],
            [['uid'], 'unique'],
            [
                ['browser_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Browser::class,
                'targetAttribute' => ['browser_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'user_id' => Yii::t('app', 'User ID'),
            'ip' => Yii::t('app', 'Ip'),
            'browser_id' => Yii::t('app', 'Browser ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'expiry_at' => Yii::t('app', 'Expiry At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrowser()
    {
        return $this->hasOne(Browser::class, ['id' => 'browser_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\UserLoginQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\UserLoginQuery(get_called_class());
    }

    /**
     * Генерирует новый токен для заданного юзера с заданной длительностью
     * Учитывает, что токен может быть сгенерирован из админки
     *
     * @param User $user модель пользователя
     * @param null|int $expiryAt время окончания сесии
     *
     * @return UserLogin
     * @throws InvalidConfigException
     */
    public static function generateForUser(User $user, ?int $expiryAt = null): UserLogin
    {
        $model = new self();
        $model->user_id = $user->getId();
        $model->generateUid();
        $model->ip = \Yii::$app->request->getRemoteIP();
        $model->browser_id = Browser::get()->id;
        $model->expiry_at = date('Y-m-d H:i:s', $expiryAt ?: time() + (3600 * 24 * 30));

        return $model;
    }

    /**
     * Генерация UID для текущей модели
     * Если запущена сессия, то как uid используется идентификатор сессии
     * если нет то генерируем md5
     *
     * @return void
     */
    public function generateUid(): void
    {
        if (Yii::$app->user->enableSession && ($uid = Yii::$app->session->getId())) {
            $this->uid = $uid;
        } else {
            $this->uid = md5(uniqid('uid', true));
        }
    }

    /**
     * Возвращает модель по Uid с проверкой на валидность
     *
     * @param string $uid Идентификатор сессии
     *
     * @return UserLogin|null
     */
    public static function getByUid(string $uid): ?UserLogin
    {
        if (!$model = self::findOne(['uid' => $uid])) {
            return null;
        }

        if (strtotime($model->expiry_at) <= time()) {
            return null;
        }

        return $model;
    }
}
