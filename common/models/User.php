<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property int $status_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $phone
 * @property int $object_id
 * @property string $blocked_at
 *
 * @property Object[] $objects
 * @property Object[] $objects0
 * @property Object $object
 */
class User extends ObjectedModel implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    public const ROLE_ADMIN = 'admin';
    public const ROLE_EMPLOYEE = 'employee';
    public const ROLE_MANAGER = 'manager';
    public const ROLE_EMPLOYEE_EVOLUTION = 'employeeEvolution';
    public const ROLE_MANAGER_EVOLUTION = 'managerEvolution';
    public const ROLE_EMPLOYEE_REALIZATION = 'employeeRealization';
    public const ROLE_MANAGER_REALIZATION = 'managerRealization';
    public const ROLE_EMPLOYEE_PRODUCTION = 'employeeProduction';
    public const ROLE_MANAGER_PRODUCTION = 'managerProduction';
    public const ROLE_EMPLOYEE_IMPORT = 'employeeImport';
    public const ROLE_MANAGER_IMPORT = 'managerImport';
    public const ROLE_EMPLOYEE_CERTIFICATION = 'employeeCertification';
    public const ROLE_MANAGER_CERTIFICATION = 'managerCertification';
    public const ROLE_EMPLOYEE_DESIGN = 'employeeDesign';
    public const ROLE_MANAGER_DESIGN = 'managerDesign';
    public const ROLE_EMPLOYEE_LOGISTICS = 'employeeLogistics';
    public const ROLE_MANAGER_LOGISTICS = 'managerLogistics';
    public const ROLE_EMPLOYEE_STOREHOUSE = 'employeeStorehouse';
    public const ROLE_MANAGER_STOREHOUSE = 'managerStorehouse';

    protected $assignments = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'status_id'], 'required'],
            [['object_id'], 'default', 'value' => null],
            [['status_id', 'object_id'], 'integer'],
            [['blocked_at'], 'safe'],
            [
                ['username', 'password_hash', 'first_name', 'last_name', 'middle_name', 'email', 'phone'],
                'string',
                'max' => 255,
            ],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [
                ['object_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => BaseObject::class,
                'targetAttribute' => ['object_id' => 'object_id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Логин'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'status_id' => Yii::t('app', 'Статус'),
            'first_name' => Yii::t('app', 'Имя'),
            'last_name' => Yii::t('app', 'Фамилия'),
            'middle_name' => Yii::t('app', 'Отчество'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Телефон'),
            'object_id' => Yii::t('app', 'Object ID'),
            'blocked_at' => Yii::t('app', 'Blocked At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(BaseObject::class, ['created_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects0()
    {
        return $this->hasMany(BaseObject::class, ['updated_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(BaseObject::class, ['object_id' => 'object_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\UserQuery(get_called_class());

    }

    public function beforeValidate()
    {
        if ($this->status_id === null) {
            $this->status_id = static::STATUS_ACTIVE;
        }

        return parent::beforeValidate();
    }

    /**
     * Добавление auth_key для всех новых юзеров
     *
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->auth_key = Yii::$app->security->generateRandomString();
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status_id' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status_id' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status_id' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status_id' => self::STATUS_INACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Создает экземпляр модели userLogin
     * {@inheritdoc}
     */
    public function afterLogin($e): void
    {
        if (!$e->cookieBased) {
            $model = UserLogin::generateForUser($e->identity, $e->duration > 0 ? time() + $e->duration : null);
            $model->save();
        }
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasAssignment($name): bool
    {
        if (!$this->assignments) {
            $this->loadAssignments();
        }

        return isset($this->assignments[$name]);
    }

    /**
     * Загружает доступные разрешения пользователя
     *
     * @return void
     */
    protected function loadAssignments(): void
    {
        $this->assignments = ArrayHelper::map(AuthAssignment::find()->where(['user_id' => $this->getId()])->all(),
            'item_name', 'item_name');
        //$this->assignments = $assignments[0] ?? [];
    }

    /**
     * @param array $assignments
     *
     * @throws \Exception
     */
    public function setAssignments(?array $assignments): void
    {
        AuthAssignment::deleteAll(['user_id' => $this->getId()]);
        /** @var OSDBManager $manager */
        $manager = Yii::$app->authManager;
        $manager->revokeAll($this->getId());
        if (!$assignments) {
            return;
        }
        foreach ($assignments as $assName => $val) {
            $manager->assign(AuthItem::findOne(['name' => $assName]), $this->getId(), $val < 0);
        }
    }

    /**
     * Возвращает «имя» пользователя на основе заполненности его данных
     *
     * @return string
     */
    public function getName(): string
    {
        if ($this->last_name || $this->first_name) {
            return implode(' ', [$this->last_name, $this->first_name]);
        }

        return $this->username;
    }

    /**
     * Возвращает «имя» пользователя на основе заполненности его данных
     *
     * @return string
     */
    public function getFullName(): string
    {
        return implode(' ', [$this->last_name, $this->first_name, $this->middle_name]);
    }

    public static function getStatusLabels(): array
    {
        return [
            static::STATUS_ACTIVE => 'Активен',
            static::STATUS_DELETED => 'Удален',
            static::STATUS_INACTIVE => 'заблокирован',
        ];
    }

    /**
     * Возвращает «имя» пользователя на основе заполненности его данных
     *
     * @return string
     */
    public function getStatusLabel(): string
    {
        $labels = static::getStatusLabels();

        return $labels[$this->status_id] ?? '-';
    }

    /**
     * Возвращает список параметров для сущности
     *
     * @return array
     */
    public static function getParams(): array
    {
        return [
            'emails',
            'phones',
        ];
    }

    /**
     * Возвращает список ролей для пользователя в человекопонятном виде
     *
     * @return string
     */
    public function getRoleDescription(): string
    {
        return implode(', ', ArrayHelper::getColumn(AuthAssignment::find()->where(['user_id' => $this->getId()])->all(),
            function (AuthAssignment $item) {
                return $item->itemName->description ?: $item->itemName->name;
            }));
    }

    /**
     * Возвращает список возможных вариантов ролей менеджера
     *
     * @return array
     */
    public static function getManagerRoles(): array
    {
        return [
            static::ROLE_MANAGER,
            static::ROLE_MANAGER_EVOLUTION,
            static::ROLE_MANAGER_REALIZATION,
            static::ROLE_MANAGER_PRODUCTION,
            static::ROLE_MANAGER_IMPORT,
            static::ROLE_MANAGER_CERTIFICATION,
            static::ROLE_MANAGER_DESIGN,
            static::ROLE_MANAGER_LOGISTICS,
            static::ROLE_MANAGER_STOREHOUSE,
        ];
    }

    /**
     * Возвращает список возможных вариантов ролей сотружника отдела
     *
     * @return array
     */
    public static function getEmployeeRoles(): array
    {
        return [
            static::ROLE_EMPLOYEE,
            static::ROLE_EMPLOYEE_EVOLUTION,
            static::ROLE_EMPLOYEE_REALIZATION,
            static::ROLE_EMPLOYEE_PRODUCTION,
            static::ROLE_EMPLOYEE_IMPORT,
            static::ROLE_EMPLOYEE_CERTIFICATION,
            static::ROLE_EMPLOYEE_DESIGN,
            static::ROLE_EMPLOYEE_LOGISTICS,
            static::ROLE_EMPLOYEE_STOREHOUSE,
        ];
    }
}
