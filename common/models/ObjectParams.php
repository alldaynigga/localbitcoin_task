<?php

namespace common\models;

use paulzi\jsonBehavior\JsonBehavior;
use paulzi\jsonBehavior\JsonValidator;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "object_params".
 *
 * @property int $id
 * @property int $object_id
 * @property int $enum_id
 * @property int $value_int
 * @property double $value_float
 * @property string $value_varchar
 * @property string $value_text
 * @property string $value_json
 *
 * @property EnumObjectParams $enum
 * @property Object $object
 */
class ObjectParams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'object_params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_id', 'enum_id', 'value_int'], 'default', 'value' => null],
            [['object_id', 'enum_id', 'value_int'], 'integer'],
            [['value_float'], 'number'],
            [['value_text'], 'string'],
            [['value_varchar'], 'string', 'max' => 255],
            [['value_json'], JsonValidator::class, 'message' => 'Значение должно быть валидным JSON объектом'],
            [
                ['enum_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => EnumObjectParams::class,
                'targetAttribute' => ['enum_id' => 'id'],
            ],
            [
                ['object_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => BaseObject::class,
                'targetAttribute' => ['object_id' => 'object_id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'enum_id' => Yii::t('app', 'Enum ID'),
            'value_int' => Yii::t('app', 'Value Int'),
            'value_float' => Yii::t('app', 'Value Float'),
            'value_varchar' => Yii::t('app', 'Value Varchar'),
            'value_text' => Yii::t('app', 'Value Text'),
            'value_json' => Yii::t('app', 'Value Json'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnum()
    {
        return $this->hasOne(EnumObjectParams::class, ['id' => 'enum_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(BaseObject::class, ['object_id' => 'object_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\ObjectParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectParamsQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => JsonBehavior::class,
                'attributes' => ['value_json'],
            ],
        ]);
    }

    /**
     * Устанавливает значение поля
     *
     * @param int $objectId id объекта модели
     * @param string $name название параметра
     * @param int|float|string $value значение параметра
     *
     * @return ObjectParams
     */
    public static function set(int $objectId, string $name, $value): ObjectParams
    {
        $enum = EnumObjectParams::get($name, EnumObjectParams::TYPE_TEXT, true);

        $model = self::findOne(['object_id' => $objectId, 'enum_id' => $enum->id]);
        if (!$model) {
            $model = new self();
            $model->object_id = $objectId;
            $model->enum_id = $enum->id;
        }

        $field = $enum->getFieldName();
        $model->$field = $value;
        $model->save();

        return $model;
    }

    /**
     * Возвращает модель параметра
     *
     * @param int $objectId id объекта модели
     * @param string $name название параметра
     * @param bool $autoCreate создаст параметр, если он не определен
     *
     * @return ObjectParams|null
     */
    public static function get(int $objectId, string $name, $autoCreate = false): ?ObjectParams
    {
        $enum = EnumObjectParams::get($name, EnumObjectParams::TYPE_TEXT, $autoCreate);
        if (!$enum) {
            return null;
        }
        $data = self::findOne(['object_id' => $objectId, 'enum_id' => $enum->id]);
        if (!$data && $autoCreate) {
            $data = new self([
                'object_id' => $objectId,
                'enum_id' => $enum->id,
            ]);
            $data->save();
        }

        return $data;
    }

    /**
     * Возвращает значение параметра
     *
     * @param int $objectId id объекта модели
     * @param string $name название параметра
     *
     * @return mixed
     */
    public static function getValue(int $objectId, string $name)
    {
        $enum = EnumObjectParams::get($name);
        if (!$enum) {
            return null;
        }
        if (!$model = self::findOne(['object_id' => $objectId, 'enum_id' => $enum->id])) {
            return false;
        }

        return $model->getAttribute($enum->getFieldName());
    }

    /**
     * Возвращает набор всех параметров для entity
     *
     * @param int $objectId id объекта модели
     *
     * @return array
     */
    public static function getValues(int $objectId): array
    {
        $data = self::find()->with('enum')->where(['object_id' => $objectId])->all();

        return ArrayHelper::map($data, 'enum.machine_name', function ($model, $default) {
            return $model[$model->enum->getFieldName()] ?? $default;
        });
    }
}
