<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_log".
 *
 * @property int $id
 * @property int $user_login_id
 * @property string $started_at
 * @property string $stopped_at
 * @property string $url
 * @property string $params
 * @property int $response_status
 * @property string $error_message
 *
 * @property UserLogin $userLogin
 * @property SystemLogDetail[] $systemLogDetails
 */
class SystemLog extends BaseLogModel
{
    public $detailModels = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_login_id', 'response_status'], 'default', 'value' => null],
            [['user_login_id', 'response_status'], 'integer'],
            [['started_at', 'stopped_at'], 'safe'],
            [['params', 'error_message'], 'string'],
            [['url'], 'string', 'max' => 1024],
            [
                ['user_login_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => UserLogin::class,
                'targetAttribute' => ['user_login_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_login_id' => Yii::t('app', 'User Login ID'),
            'started_at' => Yii::t('app', 'Started At'),
            'stopped_at' => Yii::t('app', 'Stopped At'),
            'url' => Yii::t('app', 'Url'),
            'params' => Yii::t('app', 'Params'),
            'response_status' => Yii::t('app', 'Response Status'),
            'error_message' => Yii::t('app', 'Error Message'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogin()
    {
        return $this->hasOne(UserLogin::class, ['id' => 'user_login_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSystemLogDetails()
    {
        return $this->hasMany(SystemLogDetail::class, ['system_log_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\SystemLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SystemLogQuery(get_called_class());
    }

    /**
     * Добавляет параметр в свойство attrsModels
     *
     * @param $model SystemLogDetail
     *
     * @return void
     */
    public function addDetailModel(SystemLogDetail $model): void
    {
        array_push($this->detailModels, $model);
    }

    /**
     * Возвращает массив attrsModels
     *
     * @return array
     */
    public function getDetailModels(): array
    {
        return $this->detailModels;
    }
}
