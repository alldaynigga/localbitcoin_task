<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "browser".
 *
 * @property int $id
 * @property string $device_name
 * @property string $platform
 * @property string $useragent
 * @property string $browser_name
 * @property string $browser_version
 *
 * @property UserLogin[] $userLogins
 */
class Browser extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'browser';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['device_name', 'platform', 'useragent', 'browser_name', 'browser_version'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'device_name' => Yii::t('app', 'Device Name'),
            'platform' => Yii::t('app', 'Platform'),
            'useragent' => Yii::t('app', 'Useragent'),
            'browser_name' => Yii::t('app', 'Browser Name'),
            'browser_version' => Yii::t('app', 'Browser Version'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogins()
    {
        return $this->hasMany(UserLogin::class, ['browser_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\BrowserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\BrowserQuery(get_called_class());
    }

    /**
     * Возвращает объект неопределенного браузера
     *
     * @return Browser
     */
    protected static function unknown(): self
    {
        return self::getOrCreate();
    }

    /**
     * Возвращает объект браузера
     *
     * @param string|null $userAgent Строка юзерагента. Если не передана, то будет определена автоматически
     *
     * @return Browser
     */
    public static function get(?string $userAgent = null): self
    {
        if (!$userAgent) {
            $userAgent = \Yii::$app->request->userAgent;
        }

        if (!$userAgent) {
            return self::unknown();
        }

        if (!$browser = get_browser($userAgent, true)) {
            return self::unknown();
        }

        return self::getOrCreate($userAgent, $browser['platform'] ?? 'Unknown', $browser['browser'] ?? 'Unknown',
            $browser['majorver'] ?? 'Unknown', $browser['device_type'] ?? 'Unknown');
    }


    /**
     * Возвращает объект по параметрам. Если его нет, он будет создан
     *
     * @param string $userAgent Информация о userAgent пользователя
     * @param string $platform Информация о platform пользователя
     * @param string $browserName Информация о browserName пользователя
     * @param string $browserVersion Информация о browserVersion пользователя
     * @param string $deviceType Информация о deviceType пользователя
     *
     * @return Browser
     */
    public static function getOrCreate(
        string $userAgent = 'Unknown',
        string $platform = 'Unknown',
        string $browserName = 'Unknown',
        string $browserVersion = 'Unknown',
        string $deviceType = 'Unknown'
    ): self {
        $userAgent = trim($userAgent);
        $platform = trim($platform);
        $browserName = trim($browserName);
        $browserVersion = trim($browserVersion);
        $deviceType = trim($deviceType);
        if (!$model = self::findOne(
            [
                'useragent' => $userAgent,
                'platform' => $platform,
                'browser_name' => $browserName,
                'browser_version' => $browserVersion,
                'device_name' => $deviceType,
            ]
        )
        ) {
            $model = new self();
            $model->useragent = $userAgent;
            $model->platform = $platform;
            $model->browser_name = $browserName;
            $model->browser_version = $browserVersion;
            $model->device_name = $deviceType;
            $model->save();
        }

        return $model;
    }
}
