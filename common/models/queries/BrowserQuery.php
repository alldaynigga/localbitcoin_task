<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Browser]].
 *
 * @see \common\models\Browser
 */
class BrowserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\models\Browser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Browser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
