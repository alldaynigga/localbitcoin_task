<?php

namespace common\models\queries;

use common\models\base\Entity;
use common\models\base\EntityInterface;
use common\models\base\EnumEntityParams;
use common\models\ObjectInterface;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class EntityActiveQuery
 * @package common\models\base\queries
 */
class ObjectedActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * Флаг позволяет выводить реальные записи без исключения удаленных
     *
     * @var bool
     */
    private $isDeleted = false;

    /**
     * {@inheritdoc}
     */
    public function createCommand($db = null)
    {
        if (!$this->isDeleted) {
            $this->active();
        }

        return parent::createCommand($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\storage\StorageDocument[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\storage\StorageDocument|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Позволяет управляь, включением и исключением удаленных сущностей
     * @param bool $isDeleted
     *
     * @return $this
     */
    public function isReal($isDeleted = true): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Позволяет исключить из вывода удаленные сущности
     * @return ObjectedActiveQuery
     * @throws \ReflectionException
     */
    public function active(): self
    {

        $reflection = new \ReflectionClass($this->modelClass);
        if ($reflection->implementsInterface(ObjectInterface::class)) {
            $alias = $this->getSelfAlias();

            return $this->innerJoin('object', '"object"."object_id" = ' . $alias . '."object_id"')
                ->andWhere(['object.is_deleted' => 0]);
        }

        return $this;
    }

    /**
     * Возвращает alias для построения запросов
     * @return string
     */
    protected function getSelfAlias(): string
    {
        $tableName = $this->modelClass::tableName();
        $alias = '';
        if (is_array($this->from)) {
            foreach ($this->from as $a => $select) {
                if (is_string($select)) {
                    if (strpos($select, ' ') !== false) {
                        throw new Exception(
                            \Yii::t(
                                'app',
                                'Query from can not contain spaces, Must be array or table name',
                                ['class' => static::class]
                            )
                        );
                    }
                    if ($select == $tableName) {
                        $alias = $a;
                        break;
                    }
                } elseif ($select instanceof Query) {
                    foreach ($select->from as $aSub => $selectSub) {
                        if (is_string($selectSub)) {
                            if ($selectSub == $tableName) {
                                $alias = $a;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (!$alias) {
            $alias = $tableName;
        }

        return $alias;
    }

    protected function rebuildWhere(): self
    {
        if (!empty($this->where)) {
            $this->modifyConditions($this->where);
        }

        return $this;
    }
}
