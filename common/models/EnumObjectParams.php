<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "enum_object_params".
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property int $type_id
 * @property string $class_namespace
 * @property string $created_at
 *
 * @property ObjectParams[] $objectParams
 */
class EnumObjectParams extends \yii\db\ActiveRecord
{
    public const TYPE_INT = 1;
    public const TYPE_FLOAT = 2;
    public const TYPE_VARCHAR = 3;
    public const TYPE_TEXT = 4;
    public const TYPE_FLAG = 5;
    public const TYPE_LIST = 6;
    public const TYPE_JSON = 7;
    public const TYPE_MULTY = 8;
    public const TYPE_OPTION = 9;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'enum_object_params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id'], 'default', 'value' => null],
            [['type_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'title', 'description', 'class_namespace'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'type_id' => Yii::t('app', 'Type ID'),
            'class_namespace' => Yii::t('app', 'Class Namespace'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectParams()
    {
        return $this->hasMany(ObjectParams::class, ['enum_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\EnumObjectParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\EnumObjectParamsQuery(get_called_class());
    }

    /**
     * Человекочитаемый список типов
     *
     * @return array
     */
    public static function getTypesList(): array
    {
        return [
            self::TYPE_INT => Yii::t('app', 'Целое'),
            self::TYPE_FLOAT => Yii::t('app', 'Дробное'),
            self::TYPE_VARCHAR => Yii::t('app', 'Строка'),
            self::TYPE_TEXT => Yii::t('app', 'Текст'),
            self::TYPE_FLAG => Yii::t('app', 'Флаг'),
            self::TYPE_LIST => Yii::t('app', 'Список'),
            self::TYPE_JSON => Yii::t('app', 'Поле JSON'),
            self::TYPE_MULTY => Yii::t('app', 'Множественный выбор'),
            self::TYPE_OPTION => Yii::t('app', 'Опции'),
        ];
    }

    /**
     * Человекочитаемый тип
     *
     * @return string
     */
    public function getTypeName(): string
    {
        return self::getTypesList()[$this->type_id];
    }

    /**
     * Возвращает поле по его имени.
     * Если оно ещё не определено, то будет создано
     *
     * @param string $name название поля
     * @param int $defaultType значение по умолчанию
     *
     * @return null|EnumObjectParams
     */
    public static function get(string $name, int $defaultType = self::TYPE_TEXT): ?EnumObjectParams
    {
        if (!$model = self::findOne(['name' => $name])) {
            $model = new self();
            $model->name = $name;
            $model->type_id = $defaultType;
            $model->save();
        }

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public static function getFields()
    {
        return [
            self::TYPE_INT => 'value_int',
            self::TYPE_FLOAT => 'value_float',
            self::TYPE_VARCHAR => 'value_varchar',
            self::TYPE_TEXT => 'value_text',
            self::TYPE_FLAG => 'value_int',
            self::TYPE_JSON => 'value_json',
            self::TYPE_LIST => 'value_int',
            self::TYPE_MULTY => 'value_json',
            self::TYPE_OPTION => 'value_json',
        ];
    }

    /**
     * Возвращает поле по его типу.
     * Если оно ещё не определено, то будет создано
     *
     * @return string
     */
    public function getFieldName(): string
    {
        return self::getFields()[$this->type_id];
    }

}
