<?php

namespace common\models;

use Yii;

/**
 * This is the base model class
 *
 */
class BaseLogModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function getDb()
    {
        return Yii::$app->systemLog->db;
    }
}
