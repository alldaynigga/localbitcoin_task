<?php

namespace common\models;

use yii\base\UnknownPropertyException;
use yii\db\Exception;
use yii\base\Event;
use yii\db\Expression;

/**
 * Базовый класс для "объектной" сущности.
 * "Объектная" сущность - сущность в системе, для которой хранится информация
 * о создателе, настраивается логирование, хранится общая информация
 * для всех сущностей в системе
 */
class ObjectedModel extends BaseModel implements ObjectInterface
{
    protected $attrsModels = [];

    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->on(
            static::EVENT_BEFORE_INSERT,
            function (Event $event) {
                $this->_beforeInsert($event);
                //$this->_triggers(true, $event);
            }
        );
        $this->on(
            static::EVENT_BEFORE_UPDATE,
            function (Event $event) {
                $this->_beforeUpdate($event);
                //$this->_triggers(false, $event);
            }
        );

        $this->on(
            static::EVENT_BEFORE_DELETE,
            function (Event $event) {
                $this->_beforeDelete($event);
            }
        );

        $this->on(
            static::EVENT_AFTER_FIND,
            function (Event $event) {
                $this->_afterFind($event);
            }
        );
    }

    /**
     * Возвращает id "объекта" сущности
     *
     * @return int
     */
    public function getObjectId(): ?int
    {
        return $this->object_id;
    }

    /**
     * Выдаем всем созданным классам уникальный ID
     *
     * @param Event $event событие
     *
     * @throws \Exception
     *
     * @return void
     */
    private function _beforeInsert(Event $event): void
    {
        if (empty($event->sender->object_id)) {
            $objectModel = new BaseObject();
            $objectModel->object_vocabulary_id
                = ObjectVocabulary::getIdByClassNamespace(static::getClassNamespase());
            $objectModel->created_at = new Expression('CURRENT_TIMESTAMP');
            $objectModel->created_by = $this->getCurrentUserId();
            if ($objectModel->save()) {
                $event->sender->object_id = $objectModel->object_id;
            } else {
                throw new Exception(
                    Yii::t(
                        'app',
                        'Can\'t save object for {class}',
                        ['class' => static::class]
                    )
                );
            }
            if ($event->sender->attrsModels) {
                foreach ($event->sender->attrsModels as $attrsModel) {
                    if ($attrsModel->isNewRecord) {
                        $attrsModel->object_id = $event->sender->object_id;
                        if (!$attrsModel->save()) {
                            Yii::error('Cant save param ' . $attrsModel->enum_id . ' entity ' . $event->sender->object_id);
                        }
                    }
                }
            }
        }
    }

    /**
     * Обновляем статистику
     *
     * @param Event $event событие
     *
     * @throws \Exception
     *
     * @return void
     */
    private function _beforeUpdate(Event $event): void
    {
        if (!empty($event->sender->object_id)
            &&
            $objectModel = BaseObject::find()->where(['object_id' => $this->object_id])
                ->one()
        ) {
            $objectModel = $event->sender->object;
            $userId = $this->getCurrentUserId();
            $objectModel->updated_by = $userId;
            $objectModel->updated_at = new Expression('CURRENT_TIMESTAMP');
            $objectModel->save();
        }
        if ($event->sender->attrsModels) {
            foreach ($event->sender->attrsModels as $attrsModel) {
                if ($attrsModel->isNewRecord) {
                    $attrsModel->object_id = $event->sender->object_id;
                }
                if (!$attrsModel->save()) {
                    Yii::error('Cant save param ' . $attrsModel->enum_id . ' entity ' . $event->sender->object_id);
                }
            }
        }
    }

    /**
     * Запрещаем удалять сущность и лишь выставляем ей флаг
     *
     * @param Event $event событие
     *
     * @throws \Exception
     *
     * @return void
     */
    private function _beforeDelete(Event $event): void
    {
        if (!empty($event->sender->object_id)
            &&
            $objectModel = BaseObject::find()->where(['object_id' => $this->object_id])
                ->one()
        ) {
            $objectModel = $event->sender->object;
            $objectModel->is_deleted = 1;
            $objectModel->save();
            $event->isValid = false;
        }
    }

    /**
     * @param Event $event
     */
    private function _afterFind(Event $event): void
    {
    }


    /**
     * Возвращает текущее ID юзера или null, если юзер не залогинен или не объявлен как компонент
     *
     * @return int|null
     */
    public function getCurrentUserId(): ?int
    {
        return \Yii::$app->has('user') && \Yii::$app->has('session')
            ? \Yii::$app->user->id : null;
    }

    /**
     * {@inheritdoc}
     */
    public function __get($name)
    {
        try {
            $result = parent::__get($name);
        } catch (UnknownPropertyException $exception) {
            $params = static::getParams();
            if (in_array($name, $params)) {
                return ObjectParams::getValue($this->object_id, $name);
            }
            throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
        }

        return $result;
    }

    /**
     * Устанавливает значение атрибутов и полей класса, параметров
     * {@inheritdoc}
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if ($attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes())) {
            parent::setAttributes(array_intersect_key($values, $attributes), $safeOnly);
        }
        if ($paramsAttributes = array_diff_key($values, $attributes)) {
            $this->setParamsAttributes($this, array_diff_key($values, $attributes));
        }
    }

    /**
     * Добавляет параметр в свойство attrsModels
     *
     * @param ObjectParams $attrsModel модель параметра сущности
     *
     * @return void
     */
    public function addAttrsModels(ObjectParams $attrsModel): void
    {
        array_push($this->attrsModels, $attrsModel);
    }

    /**
     * Сохраняет параметры сущности
     *
     * @param ObjectedModel $object сущность
     * @param array $data Массив параметров в виде название параметра => значение параметра
     *
     * @return void
     */
    public function setParamsAttributes(ObjectedModel $object, array $data): void
    {
        foreach (get_class($object)::getParams() as $paramName) {
            if (isset($data[$paramName]) && !empty($data[$paramName])) {
                if ($model = $object->getParamModel($paramName)) {
                    $field = $model->enum->getFieldName();
                    $model->$field = $data[$paramName];
                    $object->addAttrsModels($model);
                    unset($data[$paramName]);
                }
            }
        }
        if (!empty($data)) {
            foreach ($data as $name => $value) {
                $object->onUnsafeAttribute($name, $value);
            }
        }
    }

    /**
     * Возвращает имя класса модели
     *
     * @return string
     */
    protected static function getClassNamespase(): string
    {
        return static::class;
    }

    /**
     * Возвращает модель параметра
     *
     * @param string $param
     *
     * @return null|ObjectParams
     */
    public function getParamModel(string $param): ?ObjectParams
    {
        if (empty($this->attrsModels)) {
            $this->preloadAttrs();
        }

        if (isset($this->attrsModels[$param])) {
            return $this->attrsModels[$param];
        }

        if ($this->getId()) {
            if ($paramModel = ObjectParams::get($this->getId(), $param)) {
                return $this->attrsModels[$param] = $paramModel;
            }
        }

        if ($enum = EnumObjectParams::get($param, EnumObjectParams::TYPE_TEXT)) {
            return $this->attrsModels[$param] = new ObjectParams([
                'enum_id' => $enum->id,
            ]);
        }

        return null;
    }

    /**
     * Метод для пред-выборки всех параметров сущности, чтобы в последствии не выбирать их из базы по одной.
     * Значительно сокращает количество обращений к базе
     *
     * @return void
     */
    protected function preloadAttrs(): void
    {
        if (!$this->getId()) {
            return;
        }

        $enums = EnumObjectParams::find()->where(['name' => static::getParams()])->select('id');

        $this->attrsModels = ObjectParams::find()->andWhere([
            'object_id' => $this->getObjectId(),
            'enum_id' => $enums,
        ])->joinWith('enum')->indexBy('enum.name')->all();
    }

    /**
     * Возвращает список возможных параметров
     *
     * @return array возможные параметры
     */
    public static function getParams(): array
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
}
