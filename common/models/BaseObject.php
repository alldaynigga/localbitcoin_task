<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "object".
 *
 * @property int $object_id
 * @property string $created_at Дата создания
 * @property int $created_by
 * @property string $updated_at Последнее изменение
 * @property int $updated_by
 * @property int $is_deleted
 * @property int $object_vocabulary_id
 *
 * @property ObjectVocabulary $objectVocabulary
 * @property User $createdBy
 * @property User $updatedBy
 * @property User[] $users
 */
class BaseObject extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'is_deleted', 'object_vocabulary_id'], 'default', 'value' => null],
            [['created_by', 'updated_by', 'is_deleted', 'object_vocabulary_id'], 'integer'],
            [
                ['object_vocabulary_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ObjectVocabulary::class,
                'targetAttribute' => ['object_vocabulary_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'object_id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Создана'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'object_vocabulary_id' => Yii::t('app', 'Object Vocabulary ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectVocabulary()
    {
        return $this->hasOne(ObjectVocabulary::class, ['id' => 'object_vocabulary_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['object_id' => 'object_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\ObjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectQuery(get_called_class());
    }

    /**
     * Добавление флага is_deleted для всех новых объектов
     *{@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->is_deleted = 0;
        }

        return parent::beforeSave($insert);
    }
}
