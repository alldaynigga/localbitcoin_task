<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "system_log_detail".
 *
 * @property int $id
 * @property int $system_log_id
 * @property string $class_namespace
 * @property string $changed_attributes
 * @property int $is_saved
 * @property int $transaction_level
 * @property int $transaction_status
 *
 * @property SystemLog $systemLog
 */
class SystemLogDetail extends BaseLogModel
{
    public const TRANSACTION_STATUS_COMMIT = 1;

    public const TRANSACTION_STATUS_ROLLBACK = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_log_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['system_log_id'], 'required'],
            [['is_saved', 'transaction_level', 'transaction_status'], 'default', 'value' => null],
            [['system_log_id', 'is_saved', 'transaction_level', 'transaction_status'], 'integer'],
            [['changed_attributes'], 'string'],
            [['class_namespace'], 'string', 'max' => 255],
            [
                ['system_log_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => SystemLog::class,
                'targetAttribute' => ['system_log_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'system_log_id' => Yii::t('app', 'System Log ID'),
            'class_namespace' => Yii::t('app', 'Class Namespace'),
            'changed_attributes' => Yii::t('app', 'Changed Attributes'),
            'is_saved' => Yii::t('app', 'Is Saved'),
            'transaction_level' => Yii::t('app', 'Transaction Level'),
            'transaction_status' => Yii::t('app', 'Transaction Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSystemLog()
    {
        return $this->hasOne(SystemLog::class, ['id' => 'system_log_id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\SystemLogDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SystemLogDetailQuery(get_called_class());
    }
}
