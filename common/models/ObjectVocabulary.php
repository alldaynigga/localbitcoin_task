<?php

namespace common\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "object_vocabulary".
 *
 * @property int $id
 * @property string $class_namespace
 * @property string $name
 * @property string $table_name
 * @property string $description
 *
 * @property Object[] $objects
 */
class ObjectVocabulary extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'object_vocabulary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['class_namespace', 'name', 'table_name'], 'required'],
            [['description'], 'string'],
            [['class_namespace'], 'string', 'max' => 255],
            [['name', 'table_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['table_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'class_namespace' => Yii::t('app', 'Class Namespace'),
            'name' => Yii::t('app', 'Name'),
            'table_name' => Yii::t('app', 'Table Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Object::class, ['object_vocabulary_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\queries\ObjectVocabularyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectVocabularyQuery(get_called_class());
    }

    public static function getIdByClassNamespace($class_namespace)
    {
        if ($model = static::find()->select('id')->where(['class_namespace' => $class_namespace])->scalar()) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'Can\'t find Vocabulary for current object {class}',
            ['class' => $class_namespace]));
    }
}
