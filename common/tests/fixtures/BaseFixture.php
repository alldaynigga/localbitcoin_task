<?php

namespace common\tests\fixtures;

use yii\db\Query;
use yii\test\ActiveFixture;

class BaseFixture extends ActiveFixture
{
    /**
     * @inheritdoc
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    public function load()
    {
        $this->data = [];
        $table = $this->getTableSchema();
        $primaryKeys = [];
        foreach ($this->getData() as $alias => $row) {
            $primaryKeys = $this->db->schema->insert($table->fullName, $row);
            $this->data[$alias] = array_merge($row, $primaryKeys);
        }
        if ($table->sequenceName !== null && count($primaryKeys) > 0) {
            //recalculate sequence based on last inserted id
            $primaryKey = key($primaryKeys);
            $lastId = (new Query())->select('max(' . $primaryKey . ')')->from($table->fullName)->scalar();
            $this->db->createCommand()->resetSequence($table->fullName, $lastId + 1)->execute();
        }
    }

    public function beforeLoad()
    {
        parent::beforeLoad();
        /**
         * В случае смены pgSQL на что-то иное, это придётся ещё и переписать
         * Но на данный момент свою работу это выполняет
         */
        $this->db->createCommand('ALTER TABLE {{' . $this->getTableSchema()->fullName . '}} ENABLE TRIGGER ALL')->execute();
    }
}
