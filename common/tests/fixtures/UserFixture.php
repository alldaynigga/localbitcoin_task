<?php

namespace common\tests\fixtures;

class UserFixture extends BaseFixture
{
    public $modelClass = 'common\models\User';

    public $depends = [BaseObjectFixture::class];
}
