<?php

namespace common\tests\fixtures;

use yii\test\ActiveFixture;

class ObjectVocabularyFixture extends BaseFixture
{
    public $modelClass = 'common\models\ObjectVocabulary';
}
