<?php

namespace common\tests\fixtures;

use yii\test\ActiveFixture;

class BaseObjectFixture extends BaseFixture
{
    public $modelClass = 'common\models\BaseObject';

    public $depends = [ObjectVocabularyFixture::class];
}
