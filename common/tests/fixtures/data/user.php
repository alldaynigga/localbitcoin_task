<?php

return [
    'objected_model_test_user' => [
        'username' => 'objected_model_test_user',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'email' => 'objected_model_test_user@mail.com',
        'object_id' => 1,
    ],
    'admin' => [
        'username' => 'admin',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
        'password_hash' => \Yii::$app->security->generatePasswordHash('admin'),
        'email' => 'objected_model_test_user@mail.com',
        'object_id' => 1,
    ],
];
