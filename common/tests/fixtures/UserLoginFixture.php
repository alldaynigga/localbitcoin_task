<?php

namespace common\tests\fixtures;

use common\models\UserLogin;

class UserLoginFixture extends BaseFixture
{
    public $modelClass = UserLogin::class;
}