<?php

namespace common\tests\fixtures;

use common\models\SystemLogDetail;

class SystemLogDetailFixture extends BaseFixture
{
    public $modelClass = SystemLogDetail::class;
}