<?php

namespace common\tests\fixtures;

use common\models\SystemLog;

class SystemLogFixture extends BaseFixture
{
    public $modelClass = SystemLog::class;
}