<?php

namespace common\tests\unit\models;

use common\models\User;
use common\tests\fixtures\BaseObjectFixture;
use common\tests\fixtures\ObjectVocabularyFixture;
use Yii;
use common\models\LoginForm;
use common\tests\fixtures\UserFixture;

/**
 * Login form test
 */
class ObjectedModelTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    /**
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => UserFixture::class,
            'base_object' => [
                'class' => BaseObjectFixture::class,
            ]
            /*'object_vocabulary' => [
                'class' => ObjectVocabularyFixture::class,
                //'dataFile' => codecept_data_dir() . 'object_vocabulary.php'
            ],*/
        ];
    }

    /**
     * Проверяем создание объекта для объектной модели
     */
    public function testCreateObjectedModel()
    {
        $user = new User();
        $user->username = 'test_user';
        $user->email = 'test_user@mail.com';
        $user->setPassword('password');
        $user->generateAuthKey();
        $user->save();
        $this->assertNotEmpty($user->getObjectId());
        $objectModel = $user->object;
        $this->assertEmpty($objectModel->created_by);
        $this->assertNotEmpty($objectModel->created_at);
    }

    /**
     * Проверяем изменение объекта при изменении объектной модели
     */
    public function testUpdateObjectedModel()
    {
        $user = $this->tester->grabFixture('user', 'objected_model_test_user');
        $this->assertNotEmpty($user->getObjectId());
        $this->assertNotEmpty($user->object->updated_at);
        $updated_at = $user->object->updated_at;
        $user->save();
        $this->assertNotEquals($updated_at, $user->object->updated_at);
    }

    /**
     * Проверяем, что модель не удаляется физически, отмечается признак того,
     * что модель удалена в объекте. Проверяем, что удаленная модель
     * не возвращается в списке при поиске
     */
    public function testDeleteObjectedModel()
    {
        $user = $this->tester->grabFixture('user', 'objected_model_test_user');
        $this->assertNotEmpty($user->getObjectId());
        $this->assertNotEmpty($user->object->updated_at);
        $user->delete();
        $this->assertEquals($user->object->is_deleted, true);
        $userModel = User::find()->where(['username' => 'objected_model_test_user'])->one();
        $this->assertNull($userModel);
    }

}
