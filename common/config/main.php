<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => \common\components\rbac\OSDBManager::class,
        ],
        'systemLog' => [
            'class' => \common\components\log\SystemLogger::class,
            'db' => 'dbLog',
        ],
        'errorHandler' => [
            'class' => \common\components\ErrorHandler::class,
        ],
        'dbLog' => [
            'class' => 'yii\db\Connection',
        ],
    ],
];
