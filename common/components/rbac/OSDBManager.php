<?php

namespace common\components\rbac;

use yii\db\Expression;
use yii\db\Query;
use yii\rbac\DbManager;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;

/**
 * Class OSDBManager
 * @package common\components\rbac
 */
class OSDBManager extends DbManager
{
    protected $_checkAccessAssignments = [];
    protected $_checkDeniedAccessAssignments = [];

    /**
     * @inheritdoc
     */
    public function assign($role, $userId)
    {
        if (is_string($role)) {
            $role = new Role(
                [
                    'name' => $role,
                    'type' => Item::TYPE_ROLE,
                ]
            );
        }

        $assignment = new Assignment(
            [
                'userId' => $userId,
                'roleName' => $role->name,
                'createdAt' => new Expression('CURRENT_TIMESTAMP'),
            ]
        );

        $this->db->createCommand()->insert(
            $this->assignmentTable,
            [
                'user_id' => $assignment->userId,
                'item_name' => $assignment->roleName,
                'created_at' => $assignment->createdAt,
            ]
        )->execute();

        unset($this->_checkAccessAssignments[(string)$userId], $this->_checkDeniedAccessAssignments[(string)$userId]);

        return $assignment;
    }

    /**
     * {@inheritdoc}
     */
    protected function addItem($item)
    {
        $time = new Expression('CURRENT_TIMESTAMP');
        if ($item->createdAt === null) {
            $item->createdAt = $time;
        }
        if ($item->updatedAt === null) {
            $item->updatedAt = $time;
        }
        $this->db->createCommand()->insert(
            $this->itemTable,
            [
                'name' => $item->name,
                'type' => $item->type,
                'description' => $item->description,
                'rule_name' => $item->ruleName,
                'data' => $item->data === null ? null : serialize($item->data),
                'created_at' => $item->createdAt,
                'updated_at' => $item->updatedAt,
            ]
        )->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function updateItem($name, $item)
    {
        if ($item->name !== $name && !$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemChildTable, ['parent' => $item->name], ['parent' => $name])
                ->execute();
            $this->db->createCommand()
                ->update($this->itemChildTable, ['child' => $item->name], ['child' => $name])
                ->execute();
            $this->db->createCommand()
                ->update($this->assignmentTable, ['item_name' => $item->name], ['item_name' => $name])
                ->execute();
        }

        $item->updatedAt = new Expression('CURRENT_TIMESTAMP');

        $this->db->createCommand()->update(
            $this->itemTable,
            [
                'name' => $item->name,
                'description' => $item->description,
                'rule_name' => $item->ruleName,
                'data' => $item->data === null ? null : serialize($item->data),
                'updated_at' => $item->updatedAt,
            ],
            [
                'name' => $name,
            ]
        )->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function addRule($rule)
    {
        $time = new Expression('CURRENT_TIMESTAMP');
        if ($rule->createdAt === null) {
            $rule->createdAt = $time;
        }
        if ($rule->updatedAt === null) {
            $rule->updatedAt = $time;
        }
        $this->db->createCommand()->insert(
            $this->ruleTable,
            [
                'name' => $rule->name,
                'data' => serialize($rule),
                'created_at' => $rule->createdAt,
                'updated_at' => $rule->updatedAt,
            ]
        )->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function updateRule($name, $rule)
    {
        if ($rule->name !== $name && !$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemTable, ['rule_name' => $rule->name], ['rule_name' => $name])
                ->execute();
        }

        $rule->updatedAt = new Expression('CURRENT_TIMESTAMP');

        $this->db->createCommand()->update(
            $this->ruleTable,
            [
                'name' => $rule->name,
                'data' => serialize($rule),
                'updated_at' => $rule->updatedAt,
            ],
            [
                'name' => $name,
            ]
        )->execute();

        $this->invalidateCache();

        return true;
    }

}
