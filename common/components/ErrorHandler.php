<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components;

use Yii;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class ErrorHandler extends \yii\web\ErrorHandler
{
    /**
     * {@inheritdoc}
     */
    public function logException($exception)
    {
        if (Yii::$app->systemLog->enabled) {
            Yii::$app->systemLog->saveSystemLogException($exception);
        }
        parent::logException($exception);
    }
}
