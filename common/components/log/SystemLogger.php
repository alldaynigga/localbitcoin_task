<?php

namespace common\components\log;

use common\models\SystemLog;
use common\models\SystemLogDetail;
use common\models\UserLogin;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\db\Connection;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Application;
use yii\web\NotFoundHttpException;

/**
 * Класс для сохранения системных логов
 * Сохраняет системный лог в памяти и записывает в
 * модели по событию EVENT_AFTER_REQUEST
 */
class SystemLogger extends Component implements BootstrapInterface
{
    public const STATUS_ERROR = 500;

    public $enabled = false;

    protected $systemLog = null;

    public $db = 'db';

    public $withoutLoggingParams = [
        'password',
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::class);
    }

    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        if ($this->enabled) {
            $app->on(
                Application::EVENT_BEFORE_REQUEST,
                function () {
                    $this->createSystemLogModel();
                }
            );

            $app->on(
                Application::EVENT_AFTER_REQUEST,
                function () {
                    $this->saveSystemLogResponse();
                }
            );

            $app->db->on(
                Connection::EVENT_ROLLBACK_TRANSACTION,
                function () {
                    $this->saveTransactionStatus(SystemLogDetail::TRANSACTION_STATUS_ROLLBACK);
                }
            );
            $app->db->on(
                Connection::EVENT_COMMIT_TRANSACTION,
                function () {
                    $this->saveTransactionStatus(SystemLogDetail::TRANSACTION_STATUS_COMMIT);
                }
            );
        }
    }

    /**
     * Создает базовый экземпляр модели системного лога
     *
     * @return void
     * @throws \yii\base\Exception
     */
    public function createSystemLogModel(): void
    {
        $this->systemLog = new SystemLog();
        if (!Yii::$app->user->isGuest) {
            $this->systemLog->user_login_id = $this->getUserLoginId();
        }
        $this->systemLog->url = Yii::$app->request->hostName . Yii::$app->request->url;
        $params = ArrayHelper::merge(
            Yii::$app->request->getQueryParams(),
            Yii::$app->request->getBodyParams()
        );
        $this->unsetRequestParams($params);
        $this->systemLog->params = Json::encode($params);
    }

    /**
     * Возвращает модель системного лога
     *
     * @return SystemLog|null
     */
    public function getSystemLogModel(): ?SystemLog
    {
        if (!$this->systemLog) {
            $this->systemLog = new SystemLog();
        }

        return $this->systemLog;
    }

    /**
     * Создает базовую модель детализации системного лога
     *
     * @param string $modelClass модель в которой логируются изменения
     * @param array $dirtyAttributes массив параметро с изменениями
     * @param bool $savedStatus была ли сохранена модель
     *
     * @return void
     */
    public function createSystemLogDetail(string $modelClass, array $dirtyAttributes, bool $savedStatus): void
    {
        $logModel = $this->getSystemLogModel();
        $logDetailModel = new SystemLogDetail();
        $logDetailModel->class_namespace = $modelClass;
        $logDetailModel->changed_attributes = Json::encode($dirtyAttributes);
        $logDetailModel->is_saved = (int)$savedStatus;
        if ($transaction = Yii::$app->db->getTransaction()) {
            $logDetailModel->transaction_level = $transaction->getLevel();
        }
        $logModel->addDetailModel($logDetailModel);
    }

    /**
     * Записывает результаты запроса в лог
     *
     * @return void
     */
    public function saveSystemLogResponse(): void
    {
        $response = Yii::$app->response;
        $logModel = $this->getSystemLogModel();
        $logModel->response_status = $response->statusCode;
        if (!$response->isSuccessful) {
            $logModel->error_message = Json::encode($response->data);
        }
        $this->saveModels();
    }

    /**
     * Записывает результаты запроса в лог
     * {@inheritdoc}
     *
     * @param \Exception $exception the exception to be logged
     *
     * @return void
     */
    public function saveSystemLogException($exception): void
    {
        $logModel = $this->getSystemLogModel();
        $logModel->response_status = self::STATUS_ERROR;
        $logModel->error_message = $exception->getMessage();
        $this->saveModels();
    }

    /**
     * Сохраняет данные лога из памяти в бд
     *
     * @return void
     */
    protected function saveModels(): void
    {
        $logModel = $this->getSystemLogModel();
        $logModel->save();
        $rows = ArrayHelper::getColumn($logModel->detailModels, 'attributes');
        if (is_array($rows)) {
            foreach ($rows as &$row) {
                unset($row['id']);
                $row['system_log_id'] = $logModel->id;
            }
            $logDetailModel = new SystemLogDetail();
            $attributes = $logDetailModel->attributes();
            unset($attributes[array_search('id', $attributes)]);
            $this->db->createCommand()->batchInsert(SystemLogDetail::tableName(), $attributes, $rows)->execute();
        }
    }

    /**
     * Удаляет параметры, которые не могу быть в логе
     *
     * @param array $params параметры запроса
     *
     * @return void
     */
    protected function unsetRequestParams(array &$params): void
    {
        foreach ($params as $key => &$param) {
            if (in_array($key, $this->withoutLoggingParams)) {
                unset($params[$key]);
            }
            if (is_array($param)) {
                $this->unsetRequestParams($param);
            }
        }
    }

    /**
     * Сохраняет статус транзаций для делатлизации логов
     *
     * @param int $status статус транзакции
     *
     * @return void
     */
    public function saveTransactionStatus(int $status): void
    {
        $logModel = $this->getSystemLogModel();
        $logModel->detailModels;
        foreach ($logModel->detailModels as &$detailModel) {
            if (($detailModel->transaction_status === null) && ($detailModel->transaction_level != null)) {
                $detailModel->transaction_status = $status;
            }
        }
    }

    /**
     * Возвращает id  актуальной записи user login
     *
     * @return int
     * @throws NotFoundHttpException
     */
    protected function getUserLoginId(): int
    {
        if ($login = UserLogin::getByUid(Yii::$app->session->getId())) {
            return $login->id;
        }
        $login = UserLogin::generateForUser(Yii::$app->user->identity, time() + 7 * 24 * 60 * 60);
        if ($login->save()) {
            return $login->id;
        }
        throw new NotFoundHttpException('Can\'t find user login record');
    }
}
